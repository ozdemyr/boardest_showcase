<?php
	namespace AppBundle\Library;
	use Symfony\Component\DomCrawler\Crawler;
	
    class MetaParser
    {
		protected $url = null;
		protected $metaInfo = null;
		protected $curlInfo = null;
		protected $content = null;
		protected $contentType = null;
		protected $contentLength = 0;
		protected $httpStatus = false;
		
		public function setUrl($url){
			$this->url = $url;
		}
		
		public function getUrl(){
			return $this->url;
		}
		
		public function setMetaInfo($metaInfo){
			$this->metaInfo = $metaInfo;
		}
		
		public function getMetaInfo(){
			return $this->metaInfo;
		}
		
		public function setCurlInfo($curlInfo){
			$this->curlInfo = $curlInfo;
		}
		
		public function getCurlInfo(){
			return $this->curlInfo;
		}
		
		public function setHttpStatus($httpStatus){
			$this->httpStatus = $httpStatus;
		}
		
		public function getHttpStatus(){
			return $this->httpStatus;
		}
		
		public function setContentType($contentType){
			$this->contentType = $contentType;
		}
		
		public function getContentType(){
			return $this->contentType;
		}
		
		public function setContentLength($contentLength){
			$this->contentLength = $contentLength;
		}
		
		public function getContentLength(){
			return $this->contentLength;
		}
		
		public function setContent($content){
			$this->content = $content;
		}
		
		public function getContent(){
			return $this->content;
		}
		
		public function isImageAvailable($url){
			$curl = curl_init();
			curl_setopt($curl, CURLOPT_URL, $url);
			curl_setopt($curl, CURLOPT_FILETIME, true);
			// curl_setopt($curl, CURLOPT_NOBODY, true);
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
			curl_setopt ($curl, CURLOPT_FOLLOWLOCATION, true);
			curl_setopt($curl, CURLOPT_HEADER, true);
			curl_setopt($curl, CURLOPT_ENCODING, 'gzip, deflate'); 
			curl_setopt($curl,CURLOPT_USERAGENT,'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
			
			curl_exec($curl);
			
			$httpCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
			$contentType = curl_getinfo($curl, CURLINFO_CONTENT_TYPE);
			
			if($httpCode >= 200 && $httpCode <= 400 && strpos($contentType, 'image/') !== false){
				return true;
			}else{
				return false;
			}
		}
		
		public function fetch(){
			$curl = curl_init();
			curl_setopt($curl, CURLOPT_URL, $this->getUrl());
			curl_setopt($curl, CURLOPT_FILETIME, true);
			// curl_setopt($curl, CURLOPT_NOBODY, true);
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
			curl_setopt ($curl, CURLOPT_FOLLOWLOCATION, true);
			curl_setopt($curl, CURLOPT_HEADER, true);
			curl_setopt($curl, CURLOPT_ENCODING, 'gzip, deflate'); 
			curl_setopt($curl,CURLOPT_USERAGENT,'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
			
			$content = curl_exec($curl);
			$httpCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
			$info = curl_getinfo($curl);
			
			if($httpCode >= 200 && $httpCode <= 400){
				$content = mb_convert_encoding($content, 'HTML-ENTITIES', "UTF-8");
				$this->setHttpStatus(1);
				$this->setUrl(curl_getinfo($curl, CURLINFO_EFFECTIVE_URL));
				$this->setContent($content);
				$this->setContentType(curl_getinfo($curl, CURLINFO_CONTENT_TYPE));
				$this->setContentLength(curl_getinfo($curl, CURLINFO_CONTENT_LENGTH_DOWNLOAD));
				$this->getMetaTags();
			}else{
				$this->setHttpStatus(0);
			}
			
			curl_close($curl);
		}
		
		public function getMetaTags(){
			$html = $this->getContent();
			$crawler = new Crawler($html);
			
			$metaInfo = array(
				'base' => '',
				'icon' => '',
				'title' => '',
				'description' => '',
				'keywords' => '',
				'charset' => '',
				'http_equiv' => '',
				'twitterTags' => '',
				'openGraphTags' => '',
				'images' => '',
				'heading' => '',
				'rss' => ''
			);
			$twitterTags = array();
			$openGraphTags = array();
			
			$base = $crawler->filter('base')->eq(0);
			$icon = $crawler->filter('[rel="apple-touch-icon"], [rel="icon"]')->eq(0);
			$title = $crawler->filter('title')->eq(0);
			$description = $crawler->filter('meta[name="description"], meta[name="Description"]')->eq(0);
			$keywords = $crawler->filter('meta[name="keywords"], meta[name="Keywords"]')->eq(0);
			$charset = $crawler->filter('meta[charset]')->eq(0);
			$httpEquiv = $crawler->filter('meta[http-equiv]')->eq(0);
			$images = $crawler->filter('img[src]')->slice(0, 5);
			$heading = $crawler->filter('h1')->eq(0);
			$metaTags = $crawler->filter('meta[name], meta[property]');
			$rss = $crawler->filter('[rel="alternate"][type="application/rss+xml"]')->eq(0);
			
			$metaInfo['base'] = $base->count() ? $base->attr('href') : null;
			$metaInfo['icon'] = $icon->count() ? $icon->attr('href') : null;
			$metaInfo['title'] = $title->count() ? $title->text() : null;
			$metaInfo['description'] = $description->count() ? $description->attr('content') : null;
			$metaInfo['keywords'] = $keywords->count() ? $keywords->attr('content') : null;
			$metaInfo['charset'] = $charset->count() ? $charset->attr('charset') : null;
			$metaInfo['httpEquiv'] = $httpEquiv->count() ? $httpEquiv->attr('http_equiv') : null;
			$metaInfo['heading'] = $heading->count() ? $heading->text() : null;
			$metaInfo['rss'] = $rss->count() ? $rss->attr('href') : null;
			
			// if($images){
				// foreach($images as $image){
					// Except too long sources
					// if(strlen($image->getAttribute('src') <= 255)){
						// $metaInfo['images'][] = $image->getAttribute('src');
					// }
				// }
			// }
			
			if($metaTags){
				foreach($metaTags as $meta){
					$content = $meta->getAttribute('content');
					
					if($meta->getAttribute('name')){
						$metaAttr = $meta->getAttribute('name');
					}elseif($meta->getAttribute('property')){
						$metaAttr = $meta->getAttribute('property');
					}else{
						$metaAttr = null;					
					}
					
					if($metaAttr && strpos($metaAttr, 'og:') !== false){
						// Eğer og:image ise resmi sına. Başarılıysa ekle değilse ekleme. Parametreleri imagine hata verdiği için kaldır
						if(strtolower($metaAttr) == 'og:image'){
							if($this->isImageAvailable(strtok($content, '?'))){
								$openGraphTags[$metaAttr] = strtok($content, '?');								
							}
						}else{
							$openGraphTags[$metaAttr] = $content;
						}
					}elseif($metaAttr && strpos($metaAttr, 'twitter:') !== false){
						// Eğer og:image ise resmi sına. Başarılıysa ekle değilse ekleme. Parametreleri imagine hata verdiği için kaldır
						if(strtolower($metaAttr) == 'twitter:image'){
							if($this->isImageAvailable(strtok($content, '?'))){
								$twitterTags[$metaAttr] = strtok($content, '?');						
							}
						}else{
							$twitterTags[$metaAttr] = $content;
						}
					}
				}
			}
			
			$metaInfo['twitterTags'] = $twitterTags;
			$metaInfo['openGraphTags'] = $openGraphTags;
			
			$this->setMetaInfo($metaInfo);
		}
    }
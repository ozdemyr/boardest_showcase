<?php
	namespace AppBundle\Library;
	
	use AppBundle\Entity\UrlInfo;
	use AppBundle\Library\PhpUri;
	
    class UrlGrapher
    {
		protected $urlInfo = null;
		protected $urlGraph = array(
				'type' => '',
				'url' => '',
				'title' => '',
				'description' => '',
				'image' => '',
				'video' => '',
				'player' => '',
				'status' => array(
					'isGraphable' => false,
					'descriptions' => false,
					'title' => false,
					'description' => false,
					'image' => false,
					'video' => false,
					'player' => false
				)
			);
		
		public function __construct(UrlInfo $urlInfo){
			$this->urlInfo = $urlInfo;
			$this->resolve();
		}
		
		public function setUrl($url){
			$this->url = $url;
		}
		
		public function getUrl(){
			return $this->urlInfo->getUrl();
		}
		
		public function setMetaInfo($metaInfo){
			$this->metaInfo = $metaInfo;
		}
		
		public function getMetaInfo(){
			return $this->urlInfo->getMetaInfo();
		}
		
		public function setUrlGraph($urlGraph){
			$this->urlGraph = $urlGraph;
		}
		
		public function getGraph(){
			return $this->urlGraph;
		}
		
		function resolve(){
			$metaInfo = $this->getMetaInfo();
			$urlGraph = $this->getGraph();
			$contentType = $this->urlInfo->getContentType();
			
			/*
			 * Set type
			 */
			if(strpos($contentType, 'image/') !== false){
				$urlGraph['type'] = 'image';
			}
			 
			
			/* 
			 * Get urlGraph.url
			 */
			$urlGraph['url'] = $this->urlInfo->getUrl();
			
			/* 
			 * Get urlGraph.title
			 */
			if(isset($metaInfo['openGraphTags']['og:title'])){
				$urlGraph['title'] = $metaInfo['openGraphTags']['og:title'];
			}elseif(isset($metaInfo['twitterTags']['twitter:title'])){
				$urlGraph['title'] = $metaInfo['twitterTags']['twitter:title'];				
			}elseif(isset($metaInfo['title'])){
				$urlGraph['title'] = $metaInfo['title'];
			}
			
			/* 
			 * Get urlGraph.description
			 */
			if(isset($metaInfo['openGraphTags']['og:description'])){
				$urlGraph['description'] = $metaInfo['openGraphTags']['og:description'];
			}elseif(isset($metaInfo['description'])){
				$urlGraph['description'] = $metaInfo['description'];
			}elseif(isset($metaInfo['twitterTags']['twitter:description'])){
				$urlGraph['description'] = $metaInfo['twitterTags']['twitter:description'];				
			}
			
			/* 
			 * Get urlGraph.image
			 */
			if(isset($metaInfo['openGraphTags']['og:image'])){
				$urlGraph['image'] = $metaInfo['openGraphTags']['og:image'];
			}elseif(isset($metaInfo['twitterTags']['twitter:image'])){
				$urlGraph['image'] = $metaInfo['twitterTags']['twitter:image'];				
			}
			
			/* 
			 * Get urlGraph.video
			 */
			if(isset($metaInfo['openGraphTags']['og:video'])){
				$urlGraph['video'] = $metaInfo['openGraphTags']['og:video'];
			}elseif(isset($metaInfo['twitterTags']['twitter:player'])){
				$urlGraph['video'] = $metaInfo['twitterTags']['twitter:player'];				
			}
			
			/* 
			 * Get urlGraph.player
			 */
			if(isset($metaInfo['twitterTags']['twitter:player'])){
				$urlGraph['player'] = $metaInfo['twitterTags']['twitter:player'];				
			}elseif(isset($metaInfo['openGraphTags']['og:video'])){
				$urlGraph['player'] = $metaInfo['openGraphTags']['og:video'];
			}
			
			/*
			 * Set status
			 */
			$urlGraph['status']['image'] = !empty($urlGraph['image']) ? true : false;
			$urlGraph['status']['video'] = !empty($urlGraph['video']) ? true : false;
			$urlGraph['status']['player'] = !empty($urlGraph['player']) ? true : false;
			$urlGraph['status']['title'] = !empty($urlGraph['title']) ? true : false;
			$urlGraph['status']['description'] = !empty($urlGraph['description']) ? true : false;
			
			if($urlGraph['status']['title'] && $urlGraph['status']['description']){
				$urlGraph['status']['descriptions'] = true;
			}
			
			if($urlGraph['status']['descriptions'] || $urlGraph['status']['image']){
				$urlGraph['status']['isGraphable'] = true;
			}else{
				$urlGraph['status']['isGraphable'] = false;
			}
			 
			$this->setUrlGraph($urlGraph);
		}
    }
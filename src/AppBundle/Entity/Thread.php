<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\Encoder\EncoderAwareInterface;
use Symfony\Component\Security\Core\User\EquatableInterface;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Criteria;
use Symfony\Component\Validator\Constraints as Assert;
use AppBundle\Validator\Constraints as CustomAssert;

/**
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="AppBundle\Entity\ThreadRepository")
 * @ORM\Table(name="b_thread")
 * @ORM\HasLifecycleCallbacks
 */
class Thread
{

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     * @Assert\Length(max=255)
     * @CustomAssert\ContainsAtLeastOneAlpha
     */
    protected $title;

    /**
     * Bidirectional - Many Comments are authored by one user (OWNING SIDE)
     * @ORM\ManyToOne(targetEntity="User", inversedBy="threadsAuthored")
     * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
     * @Assert\NotBlank()
     * 
     */
    protected $author;
    
    /**
     * @ORM\ManyToOne(targetEntity="TopicTitle")
     * @ORM\JoinColumn(nullable=true, onDelete="SET NULL")
     */
    protected $topicTitle;

    /**
     * @Gedmo\Slug(fields={"title"}, updatable=true)
     * @ORM\Column(length=132, type="string", unique=true)
     */
    protected $slug;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Assert\Url(
     *    protocols = {"http", "https", "ftp"}
     * )
     */
    protected $link;
	
    /**
     * @ORM\OneToOne(targetEntity="UrlInfo", mappedBy="thread")
     * @ORM\JoinColumn(nullable=true, onDelete="SET NULL")
     */
    private $urlInfo;

    /**
     * @ORM\Column(type="text", length=30000, nullable=true)
     * @Assert\Length(max=30000)
     */
    protected $content;

    /**
     * @ORM\Column(type="string", length=16)
     * @Assert\NotBlank()
     * @Assert\Choice(choices = {"en", "tr", "es", "ru", "fr", "de"})
     */
    protected $language = 'en';

    /**
     * @ORM\Column(type="boolean", options={"default" = 1})
     */
    protected $status;

    /**
     * @ORM\Column(type="string", length=16)
     * @Assert\NotBlank()
     * @Assert\Choice(choices = {"question", "link", "thread"})
     */
    protected $type;

    /**
     * @ORM\Column(type="string", length=32, nullable=true)
     * @Assert\Choice(choices = {"", "sponsored", "sponsored-topic", "announcement", "announcement-topic"})
     */
    protected $pinType;
	
    /**
     * @ORM\Column(type="string", length=1024, nullable=true)
     * @Assert\Length(max=1024)
     */
    protected $modNote;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $addDate;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $editDate;
    
    /**
     * @var \Doctrine\Common\Collections\Collection|Topic[]
     *
     * @ORM\ManyToMany(targetEntity="Topic", inversedBy="threads")
     * @ORM\JoinTable(
     *  name="b_thread_topic_relation"
     * )
     */
    protected $topics;

    /**
     * Bidirectional - One-To-Many (INVERSE SIDE)
     * @ORM\OneToMany(targetEntity="Reply", mappedBy="thread", fetch="EXTRA_LAZY")
     */
    protected $replies;

    /**
     * Bidirectional - One-To-Many (INVERSE SIDE)
     * @ORM\OneToMany(targetEntity="ThreadUpvote", mappedBy="thread", fetch="EXTRA_LAZY")
     */
    protected $upvotes;

    /**
     * Bidirectional - One-To-Many (INVERSE SIDE)
     * @ORM\OneToMany(targetEntity="ThreadDownvote", mappedBy="thread", fetch="EXTRA_LAZY")
     */
    protected $downvotes;

    /**
     * Bidirectional - One-To-Many (INVERSE SIDE)
     * @ORM\OneToMany(targetEntity="ThreadFollow", mappedBy="thread", fetch="EXTRA_LAZY")
     */
    protected $followers;
    
    protected $host;
	
    protected $hostType;
	
    protected $alternateLink;
	
    protected $mediaBundle;
	
    protected $linkType;
    
    /** 
     * @ORM\PostLoad 
     */
    public function doStuffOnPostLoad()
    {
        if($this->getLink()){
			$parsed_url = parse_url($this->getLink());
            $this->host = $parsed_url['host'];
			
			/* Is it YouTube video link?
			 * Örnek link yapısı şu şekildedir
			 * https://www.youtube.com/watch?v=OqPnLdky0EI
			 * veya https://youtu.be/OqPnLdky0EI
			 */
			if(in_array($parsed_url['host'], array('youtube.com', 'www.youtube.com', 'youtu.be', 'www.youtu.be'))){
				
				$query_params = array();
				if(isset($parsed_url['query'])){
					parse_str($parsed_url['query'], $query_params);
				}else{
					$parsed_url['query'] = null;
				}
				
				if(
					// Eğer link anadomaine aitse
					(in_array($parsed_url['host'], array('youtube.com', 'www.youtube.com'))
					&&
					(isset($parsed_url['path']) && strpos($parsed_url['path'], '/watch') == 0)
					&& 
					array_key_exists('v', $query_params)
					&& 
					!empty($query_params['v']))
					// Eğer link youtu.be domainine aitse
					||
					(in_array($parsed_url['host'], array('youtu.be','www.youtu.be')) && isset($parsed_url['path']) && !empty($parsed_url['path']))
				){
					$this->setHostType('youtube');
					$this->setLinkType('video');
					
					if(in_array($parsed_url['host'], array('youtube.com', 'www.youtube.com'))){
						// örnek adres https://youtube.com/embed/OqPnLdky0EI?autoplay=1
						$this->setAlternateLink('https://youtube.com/embed/' . $query_params['v'] . '?autoplay=1');
					}elseif(in_array($parsed_url['host'], array('youtu.be','www.youtu.be'))){
						$this->setAlternateLink('https://youtube.com/embed/' . $parsed_url['path'] . '?autoplay=1');
					}
					
				}
			}
			
			/* Is it Vimeo video link?
			 * Örnek link yapısı şu şekildedir
			 * https://vimeo.com/189530509
			 */ 
			if(in_array($parsed_url['host'], array('vimeo.com', 'www.vimeo.com')) && isset($parsed_url['path'])){
				$query_params = array();
				if(isset($parsed_url['query'])){
					parse_str($parsed_url['query'], $query_params);
				}else{
					$parsed_url['query'] = null;
				}				
				
				$vimeoId = (int) str_replace('/', '', $parsed_url['path']);
				
				if(is_int($vimeoId)){
					$this->setHostType('vimeo');
					
					// örnek adres //player.vimeo.com/video/189530509?autoplay=1
					$this->setAlternateLink('//player.vimeo.com/video/' . $vimeoId . '?autoplay=1');
				}
			}
			
			/* Is it Dailymotion video link?
			 * Örnek link yapısı şu şekildedir
			 * http://www.dailymotion.com/video/x51hrws_vatanim-sensin-mustafa-kemal-sahnesi_tv
			 */ 
			if(in_array($parsed_url['host'], array('dailymotion.com', 'www.dailymotion.com')) && isset($parsed_url['path']) && strpos($parsed_url['path'], '/video/') !== -1){
				$str = str_replace('/video/', '', $parsed_url['path']);
				
				if(!empty($str)){
					// $this->setHostType('dailymotion');
				}
			}
			
        }else{
			$this->host = null;
		}
    }
	
	public function getHost(){
		return $this->host;
	}
	
	public function setHost($host){
		$this->host = $host;
	}
	
	public function getHostType(){
		return $this->hostType;
	}
	
	public function setHostType($hostType){
		$this->hostType = $hostType;
	}
	
	public function getAlternateLink(){
		return $this->alternateLink;
	}
	
	public function setAlternateLink($alternateLink){
		$this->alternateLink = $alternateLink;
	}
	
	public function getLinkType(){
		return $this->linkType;
	}
	
	public function setLinkType($linkType){
		$this->linkType = $linkType;
	}

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->topics = new \Doctrine\Common\Collections\ArrayCollection();
        $this->replies = new \Doctrine\Common\Collections\ArrayCollection();
        $this->upvotes = new \Doctrine\Common\Collections\ArrayCollection();
        $this->downvotes = new \Doctrine\Common\Collections\ArrayCollection();
        $this->followers = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Thread
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return Thread
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set link
     *
     * @param string $link
     *
     * @return Thread
     */
    public function setLink($link)
    {
        $this->link = $link;

        return $this;
    }

    /**
     * Get link
     *
     * @return string
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * Set content
     *
     * @param string $content
     *
     * @return Thread
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set language
     *
     * @param string $language
     *
     * @return Thread
     */
    public function setLanguage($language)
    {
        $this->language = $language;

        return $this;
    }

    /**
     * Get language
     *
     * @return string
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * Set status
     *
     * @param boolean $status
     *
     * @return Thread
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return boolean
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return Thread
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set addDate
     *
     * @param \DateTime $addDate
     *
     * @return Thread
     */
    public function setAddDate($addDate)
    {
        $this->addDate = $addDate;

        return $this;
    }

    /**
     * Get addDate
     *
     * @return \DateTime
     */
    public function getAddDate()
    {
        return $this->addDate;
    }

    /**
     * Set editDate
     *
     * @param \DateTime $editDate
     *
     * @return Thread
     */
    public function setEditDate($editDate)
    {
        $this->editDate = $editDate;

        return $this;
    }

    /**
     * Get editDate
     *
     * @return \DateTime
     */
    public function getEditDate()
    {
        return $this->editDate;
    }

    /**
     * Set author
     *
     * @param \AppBundle\Entity\User $author
     *
     * @return Thread
     */
    public function setAuthor(\AppBundle\Entity\User $author)
    {
        $this->author = $author;

        return $this;
    }

    /**
     * Get author
     *
     * @return \AppBundle\Entity\User
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * Set topicTitle
     *
     * @param \AppBundle\Entity\TopicTitle $topicTitle
     *
     * @return Thread
     */
    public function setTopicTitle(\AppBundle\Entity\TopicTitle $topicTitle = null)
    {
        $this->topicTitle = $topicTitle;

        return $this;
    }

    /**
     * Get topicTitle
     *
     * @return \AppBundle\Entity\TopicTitle
     */
    public function getTopicTitle()
    {
        return $this->topicTitle;
    }

    /**
     * Add topic
     *
     * @param \AppBundle\Entity\Topic $topic
     *
     * @return Thread
     */
    public function addTopic(\AppBundle\Entity\Topic $topic)
    {
        $this->topics[] = $topic;

        return $this;
    }

    /**
     * Remove topic
     *
     * @param \AppBundle\Entity\Topic $topic
     */
    public function removeTopic(\AppBundle\Entity\Topic $topic)
    {
        $this->topics->removeElement($topic);
    }

    /**
     * Get topics
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTopics()
    {
        return $this->topics;
    }

    /**
     * Add reply
     *
     * @param \AppBundle\Entity\Reply $reply
     *
     * @return Thread
     */
    public function addReply(\AppBundle\Entity\Reply $reply)
    {
        $this->replies[] = $reply;

        return $this;
    }

    /**
     * Remove reply
     *
     * @param \AppBundle\Entity\Reply $reply
     */
    public function removeReply(\AppBundle\Entity\Reply $reply)
    {
        $this->replies->removeElement($reply);
    }

    /**
     * Get replies
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getReplies()
    {
        return $this->replies;
    }

    /**
     * Add upvote
     *
     * @param \AppBundle\Entity\ThreadUpvote $upvote
     *
     * @return Thread
     */
    public function addUpvote(\AppBundle\Entity\ThreadUpvote $upvote)
    {
        $this->upvotes[] = $upvote;

        return $this;
    }

    /**
     * Remove upvote
     *
     * @param \AppBundle\Entity\ThreadUpvote $upvote
     */
    public function removeUpvote(\AppBundle\Entity\ThreadUpvote $upvote)
    {
        $this->upvotes->removeElement($upvote);
    }

    /**
     * Get upvotes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUpvotes()
    {
        return $this->upvotes;
    }

    /**
     * Add downvote
     *
     * @param \AppBundle\Entity\ThreadDownvote $downvote
     *
     * @return Thread
     */
    public function addDownvote(\AppBundle\Entity\ThreadDownvote $downvote)
    {
        $this->downvotes[] = $downvote;

        return $this;
    }

    /**
     * Remove downvote
     *
     * @param \AppBundle\Entity\ThreadDownvote $downvote
     */
    public function removeDownvote(\AppBundle\Entity\ThreadDownvote $downvote)
    {
        $this->downvotes->removeElement($downvote);
    }

    /**
     * Get downvotes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDownvotes()
    {
        return $this->downvotes;
    }

    /**
     * Add follower
     *
     * @param \AppBundle\Entity\ThreadFollow $follower
     *
     * @return Thread
     */
    public function addFollower(\AppBundle\Entity\ThreadFollow $follower)
    {
        $this->followers[] = $follower;

        return $this;
    }

    /**
     * Remove follower
     *
     * @param \AppBundle\Entity\ThreadFollow $follower
     */
    public function removeFollower(\AppBundle\Entity\ThreadFollow $follower)
    {
        $this->followers->removeElement($follower);
    }

    /**
     * Get followers
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFollowers()
    {
        return $this->followers;
    }
	
    public function setMediaBundle($mediaBundle)
    {
        $this->mediaBundle = $mediaBundle;
		
		return $this;
    }
	
	public function getMediaBundle()
	{
		$allContent = $this->getContent();
		
		foreach($this->getReplies() as $reply){
			$allContent .= $reply->getContent();
		}
		
		if(!$allContent){return null;}
		
		$dom = new \DOMDocument();
		$dom->loadHTML($allContent);
		
		$firstImage = $dom->getElementsByTagName('img')->item(0);
		
		if($firstImage !== null){
			$firstImage = $firstImage->getAttribute('src');
		}
		
		return $firstImage;
	}

    /**
     * Set pinType
     *
     * @param string $pinType
     *
     * @return Thread
     */
    public function setPinType($pinType)
    {
        $this->pinType = $pinType;

        return $this;
    }

    /**
     * Get pinType
     *
     * @return string
     */
    public function getPinType()
    {
        return $this->pinType;
    }

    /**
     * Set modNote
     *
     * @param string $modNote
     *
     * @return Thread
     */
    public function setModNote($modNote)
    {
        $this->modNote = $modNote;

        return $this;
    }

    /**
     * Get modNote
     *
     * @return string
     */
    public function getModNote()
    {
        return $this->modNote;
    }

    /**
     * Set urlInfo
     *
     * @param \AppBundle\Entity\UrlInfo $urlInfo
     *
     * @return Thread
     */
    public function setUrlInfo(\AppBundle\Entity\UrlInfo $urlInfo = null)
    {
        $this->urlInfo = $urlInfo;

        return $this;
    }

    /**
     * Get urlInfo
     *
     * @return \AppBundle\Entity\UrlInfo
     */
    public function getUrlInfo()
    {
        return $this->urlInfo;
    }
}

<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\Encoder\EncoderAwareInterface;
use Symfony\Component\Security\Core\User\EquatableInterface;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Criteria;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="AppBundle\Entity\ReplyRepository")
 * @ORM\Table(name="b_reply")
 * @ORM\HasLifecycleCallbacks
 */
class Reply
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="Thread", inversedBy="replies")
     * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
     * @Assert\NotBlank()
     */
    protected $thread;

    /**
     * @ORM\ManyToOne(targetEntity="Reply", inversedBy="replies")
     * @ORM\JoinColumn(nullable=true, onDelete="CASCADE")
     */
    protected $reply;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="repliesAuthored")
     * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
     * @Assert\NotBlank()
     */
    protected $author;

    /**
     * @ORM\ManyToOne(targetEntity="TopicTitle")
     * @ORM\JoinColumn(nullable=true, onDelete="SET NULL")
     */
    protected $topicTitle;

    /**
     * @ORM\Column(type="text", length=30000)
     * @Assert\NotBlank()
     */
    protected $content;
	
    /**
     * @ORM\Column(type="text", length=16)
     * @Assert\NotBlank()
     * @Assert\Choice(choices = {"thread", "reply"})
     */
    protected $type;

    /**
     * @ORM\Column(type="boolean")
     * @Assert\NotBlank()
     */
    protected $status;
	
    /**
     * @ORM\Column(type="string", length=128, nullable=true)
     * @Assert\Length(max=128)
     */
    protected $modNote;

    /**
     * @ORM\Column(type="boolean", options={"default" = 0})
     */
    protected $isQualified = 0;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $addDate;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $editDate;

    /**
     * @ORM\OneToMany(targetEntity="Reply", mappedBy="reply", fetch="EXTRA_LAZY")
     */
    private $replies;
  
    /**
     * Bidirectional - One-To-Many (INVERSE SIDE)
     * @ORM\OneToMany(targetEntity="ReplyUpvote", mappedBy="reply", fetch="EXTRA_LAZY")
     */
    protected $upvotes;
  
    /**
     * Bidirectional - One-To-Many (INVERSE SIDE)
     * @ORM\OneToMany(targetEntity="ReplyDownvote", mappedBy="reply", fetch="EXTRA_LAZY")
     */
    protected $downvotes;
    
    /** 
     * @ORM\PostLoad 
     */
    public function doStuffOnPostLoad()
    {
        
    }
    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->replies = new \Doctrine\Common\Collections\ArrayCollection();
        $this->upvotes = new \Doctrine\Common\Collections\ArrayCollection();
        $this->downvotes = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set content
     *
     * @param string $content
     *
     * @return Reply
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return Reply
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set status
     *
     * @param integer $status
     *
     * @return Reply
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set addDate
     *
     * @param \DateTime $addDate
     *
     * @return Reply
     */
    public function setAddDate($addDate)
    {
        $this->addDate = $addDate;

        return $this;
    }

    /**
     * Get addDate
     *
     * @return \DateTime
     */
    public function getAddDate()
    {
        return $this->addDate;
    }

    /**
     * Set editDate
     *
     * @param \DateTime $editDate
     *
     * @return Reply
     */
    public function setEditDate($editDate)
    {
        $this->editDate = $editDate;

        return $this;
    }

    /**
     * Get editDate
     *
     * @return \DateTime
     */
    public function getEditDate()
    {
        return $this->editDate;
    }

    /**
     * Set thread
     *
     * @param \AppBundle\Entity\Thread $thread
     *
     * @return Reply
     */
    public function setThread(\AppBundle\Entity\Thread $thread)
    {
        $this->thread = $thread;

        return $this;
    }

    /**
     * Get thread
     *
     * @return \AppBundle\Entity\Thread
     */
    public function getThread()
    {
        return $this->thread;
    }

    /**
     * Set reply
     *
     * @param \AppBundle\Entity\Reply $reply
     *
     * @return Reply
     */
    public function setReply(\AppBundle\Entity\Reply $reply = null)
    {
        $this->reply = $reply;

        return $this;
    }

    /**
     * Get reply
     *
     * @return \AppBundle\Entity\Reply
     */
    public function getReply()
    {
        return $this->reply;
    }

    /**
     * Set author
     *
     * @param \AppBundle\Entity\User $author
     *
     * @return Reply
     */
    public function setAuthor(\AppBundle\Entity\User $author)
    {
        $this->author = $author;

        return $this;
    }

    /**
     * Get author
     *
     * @return \AppBundle\Entity\User
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * Add reply
     *
     * @param \AppBundle\Entity\Reply $reply
     *
     * @return Reply
     */
    public function addReply(\AppBundle\Entity\Reply $reply)
    {
        $this->replies[] = $reply;

        return $this;
    }

    /**
     * Remove reply
     *
     * @param \AppBundle\Entity\Reply $reply
     */
    public function removeReply(\AppBundle\Entity\Reply $reply)
    {
        $this->replies->removeElement($reply);
    }

    /**
     * Get replies
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getReplies()
    {
        return $this->replies;
    }
    
    /**
     * Set topicTitle
     *
     * @param \AppBundle\Entity\TopicTitle $topicTitle
     *
     * @return Reply
     */
    public function setTopicTitle(\AppBundle\Entity\TopicTitle $topicTitle = null)
    {
        $this->topicTitle = $topicTitle;

        return $this;
    }

    /**
     * Get topicTitle
     *
     * @return \AppBundle\Entity\TopicTitle
     */
    public function getTopicTitle()
    {
        return $this->topicTitle;
    }

    /**
     * Add upvote
     *
     * @param \AppBundle\Entity\ReplyUpvote $upvote
     *
     * @return Reply
     */
    public function addUpvote(\AppBundle\Entity\ReplyUpvote $upvote)
    {
        $this->upvotes[] = $upvote;

        return $this;
    }

    /**
     * Remove upvote
     *
     * @param \AppBundle\Entity\ReplyUpvote $upvote
     */
    public function removeUpvote(\AppBundle\Entity\ReplyUpvote $upvote)
    {
        $this->upvotes->removeElement($upvote);
    }

    /**
     * Get upvotes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUpvotes()
    {
        return $this->upvotes;
    }

    /**
     * Add downvote
     *
     * @param \AppBundle\Entity\ReplyDownvote $downvote
     *
     * @return Reply
     */
    public function addDownvote(\AppBundle\Entity\ReplyDownvote $downvote)
    {
        $this->downvotes[] = $downvote;

        return $this;
    }

    /**
     * Remove downvote
     *
     * @param \AppBundle\Entity\ReplyDownvote $downvote
     */
    public function removeDownvote(\AppBundle\Entity\ReplyDownvote $downvote)
    {
        $this->downvotes->removeElement($downvote);
    }

    /**
     * Get downvotes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDownvotes()
    {
        return $this->downvotes;
    }

    /**
     * Set modNote
     *
     * @param string $modNote
     *
     * @return Reply
     */
    public function setModNote($modNote)
    {
        $this->modNote = $modNote;

        return $this;
    }

    /**
     * Get modNote
     *
     * @return string
     */
    public function getModNote()
    {
        return $this->modNote;
    }

    /**
     * Set isQualified
     *
     * @param boolean $isQualified
     *
     * @return Reply
     */
    public function setIsQualified($isQualified)
    {
        $this->isQualified = $isQualified;

        return $this;
    }

    /**
     * Get isQualified
     *
     * @return boolean
     */
    public function getIsQualified()
    {
        return $this->isQualified;
    }
}

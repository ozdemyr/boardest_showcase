<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="AppBundle\Entity\PrivateMessageRepository")
 * @ORM\Table(name="b_private_message")
 */
class PrivateMessage
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * Bidirectional - Many Comments are authored by one user (OWNING SIDE)
     * @ORM\ManyToOne(targetEntity="User", inversedBy="privateMessagesSent")
     * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
     * @Assert\NotBlank()
     * 
     */
    protected $sender;
	
    /**
     * Bidirectional - Many Comments are authored by one user (OWNING SIDE)
     * @ORM\ManyToOne(targetEntity="User", inversedBy="privateMessagesReceived")
     * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
     * @Assert\NotBlank()
     * 
     */
    protected $recipient;

    /**
     * @ORM\Column(type="text", length=30000)
     * @Assert\NotBlank()
     */
    protected $content;
    
    /**
     * @ORM\Column(type="datetime")
     */
    protected $addDate;
    
    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $viewDate;

    /**
     * @ORM\Column(type="boolean", options={"default" = 0})
     */
    protected $isSeen;

    /**
     * @ORM\Column(type="boolean", options={"default" = 0})
     */
    protected $isRead;

    /**
     * @ORM\Column(type="boolean", options={"default" = 0})
     */
    protected $isNotificated;
	
    /**
     * @ORM\Column(type="text", length=32)
     * @Assert\NotBlank()
     * @Assert\Choice(choices = {"open", "deleted-by-sender", "deleted-by-recipient", "deleted"})
     */
    protected $status;
	
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set addDate
     *
     * @param \DateTime $addDate
     *
     * @return PrivateMessage
     */
    public function setAddDate($addDate)
    {
        $this->addDate = $addDate;

        return $this;
    }

    /**
     * Get addDate
     *
     * @return \DateTime
     */
    public function getAddDate()
    {
        return $this->addDate;
    }

    /**
     * Set viewDate
     *
     * @param \DateTime $viewDate
     *
     * @return PrivateMessage
     */
    public function setViewDate($viewDate)
    {
        $this->viewDate = $viewDate;

        return $this;
    }

    /**
     * Get viewDate
     *
     * @return \DateTime
     */
    public function getViewDate()
    {
        return $this->viewDate;
    }

    /**
     * Set isSeen
     *
     * @param boolean $isSeen
     *
     * @return PrivateMessage
     */
    public function setIsSeen($isSeen)
    {
        $this->isSeen = $isSeen;

        return $this;
    }

    /**
     * Get isSeen
     *
     * @return boolean
     */
    public function getIsSeen()
    {
        return $this->isSeen;
    }

    /**
     * Set isRead
     *
     * @param boolean $isRead
     *
     * @return PrivateMessage
     */
    public function setIsRead($isRead)
    {
        $this->isRead = $isRead;

        return $this;
    }

    /**
     * Get isRead
     *
     * @return boolean
     */
    public function getIsRead()
    {
        return $this->isRead;
    }

    /**
     * Set sender
     *
     * @param \AppBundle\Entity\User $sender
     *
     * @return PrivateMessage
     */
    public function setSender(\AppBundle\Entity\User $sender)
    {
        $this->sender = $sender;

        return $this;
    }

    /**
     * Get sender
     *
     * @return \AppBundle\Entity\User
     */
    public function getSender()
    {
        return $this->sender;
    }

    /**
     * Set recipient
     *
     * @param \AppBundle\Entity\User $recipient
     *
     * @return PrivateMessage
     */
    public function setRecipient(\AppBundle\Entity\User $recipient)
    {
        $this->recipient = $recipient;

        return $this;
    }

    /**
     * Get recipient
     *
     * @return \AppBundle\Entity\User
     */
    public function getRecipient()
    {
        return $this->recipient;
    }

    /**
     * Set content
     *
     * @param string $content
     *
     * @return PrivateMessage
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return PrivateMessage
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set isNotificated
     *
     * @param boolean $isNotificated
     *
     * @return PrivateMessage
     */
    public function setIsNotificated($isNotificated)
    {
        $this->isNotificated = $isNotificated;

        return $this;
    }

    /**
     * Get isNotificated
     *
     * @return boolean
     */
    public function getIsNotificated()
    {
        return $this->isNotificated;
    }
}

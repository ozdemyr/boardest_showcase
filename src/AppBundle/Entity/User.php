<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\AdvancedUserInterface;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints as Assert;
use AppBundle\Validator\Constraints as CustomAssert;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="AppBundle\Entity\UserRepository")
 * @UniqueEntity("email")
 * @ORM\Table(name="b_user")
 * @Vich\Uploadable
 * @ORM\HasLifecycleCallbacks
 */
class User implements AdvancedUserInterface, \Serializable
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=32))
     * @Assert\NotBlank()
     * @Assert\Length(min=2, max=32)
     * @CustomAssert\ContainsAlpha
     * @CustomAssert\ContainsAtLeastOneAlpha
     */
    protected $name;

    /**
     * @ORM\Column(type="string", length=128, unique=true)
     * @Assert\Email()
     */
    protected $email;

    /**
     * @ORM\Column(type="string", length=128, nullable=true)
     */
    protected $activationCode;

    /**
     * @Gedmo\Slug(fields={"name"}, updatable=false, separator="")
     * @ORM\Column(type="string", unique=true, length=128)
     */
    protected $username;

    /**
     * @ORM\Column(type="string", length=128)
     * @Assert\NotBlank()
     * @Assert\Length(min=6, max=128)
     */
    protected $password;

    /**
     * @ORM\Column(type="string", nullable=true, length=80)
     * @Assert\Length(max=80)
     */
    protected $title;

    /**
     * @Vich\UploadableField(mapping="user_avatar", fileNameProperty="avatar")
     * @Assert\Image(maxSize="8M")
     * @var File
     */
    private $imageFile;
    
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Length(max=255)
     */
    protected $avatar;

    /**
     * @ORM\Column(type="string", length=1024, nullable=true)
     * @Assert\Length(max=1024)
     */
    protected $about;

    /**
     * @ORM\Column(type="boolean", options={"default" = 0})
     */
    protected $isActive;

    /**
     * @ORM\Column(type="boolean", options={"default" = 0})
     */
    protected $isBanned = 0;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Length(max=255)
     */
    protected $banReason;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $registerDate;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $editDate;

    /**
     * @ORM\Column(type="array")
     */
    protected $roles;
	
    /**
     * @ORM\Column(type="string", length=32)
     * @Assert\NotBlank()
     * @Assert\Choice(choices = {"open", "closed", "usersfollowed"})
     */
    protected $acceptMessages = 'open';
	
    /**
     * @ORM\Column(type="string", length=16, nullable=true)
     */
    protected $browserLocale;

    /**
     * Bidirectional - One-To-Many (INVERSE SIDE)
     * @ORM\OneToMany(targetEntity="TopicTitle", mappedBy="owner")
     */
    protected $titles;
	
    /**
     * Bidirectional - One-To-Many (INVERSE SIDE)
     * @ORM\OneToMany(targetEntity="Thread", mappedBy="author", fetch="EXTRA_LAZY")
     */
    protected $threadsAuthored;
    
    /**
     * Bidirectional - One-To-Many (INVERSE SIDE)
     * @ORM\OneToMany(targetEntity="Reply", mappedBy="author", fetch="EXTRA_LAZY")
     */
    protected $repliesAuthored;
    
    /**
     * Bidirectional - One-To-Many (INVERSE SIDE)
     * @ORM\OneToMany(targetEntity="ThreadDraft", mappedBy="author", fetch="EXTRA_LAZY")
     */
    protected $threadDraftsAuthored;
	
    /**
     * Bidirectional - One-To-Many (INVERSE SIDE)
     * @ORM\OneToMany(targetEntity="ReplyDraft", mappedBy="author", fetch="EXTRA_LAZY")
     */
    protected $replyDraftsAuthored;

    /**
     * Bidirectional - One-To-Many (INVERSE SIDE)
     * @ORM\OneToMany(targetEntity="ThreadUpvote", mappedBy="owner")
     */
    protected $threadsUpvoted;

    /**
     * Bidirectional - One-To-Many (INVERSE SIDE)
     * @ORM\OneToMany(targetEntity="ThreadDownvote", mappedBy="owner")
     */
    protected $threadsDownvoted;
	
    /**
     * Bidirectional - One-To-Many (INVERSE SIDE)
     * @ORM\OneToMany(targetEntity="ThreadView", mappedBy="owner", fetch="EXTRA_LAZY")
     */
    protected $threadVisits;

    /**
     * Bidirectional - One-To-Many (INVERSE SIDE)
     * @ORM\OneToMany(targetEntity="ReplyUpvote", mappedBy="owner")
     */
    protected $repliesUpvoted;

    /**
     * Bidirectional - One-To-Many (INVERSE SIDE)
     * @ORM\OneToMany(targetEntity="ReplyDownvote", mappedBy="owner")
     */
    protected $repliesDownvoted;

    /**
     * Bidirectional - One-To-Many (INVERSE SIDE)
     * @ORM\OneToMany(targetEntity="ThreadFollow", mappedBy="owner")
	 * @ORM\OrderBy({"addDate" = "DESC"})
     */
    protected $threadsFollowed;

    /**
     * Bidirectional - One-To-Many (INVERSE SIDE)
     * @ORM\OneToMany(targetEntity="UserFollow", mappedBy="owner", fetch="EXTRA_LAZY")
     */
    protected $usersFollowed;

    /**
     * Bidirectional - One-To-Many (INVERSE SIDE)
     * @ORM\OneToMany(targetEntity="TopicFollow", mappedBy="owner", fetch="EXTRA_LAZY")
     */
    protected $topicsFollowed;

    /**
     * Bidirectional - One-To-Many (INVERSE SIDE)
     * @ORM\OneToMany(targetEntity="UserFollow", mappedBy="user")
     */
    protected $followers;
	
    /**
     * Bidirectional - One-To-Many (INVERSE SIDE)
     * @ORM\OneToMany(targetEntity="RoleForTopic", mappedBy="user", fetch="EXTRA_LAZY", cascade={"persist"})
     */
    protected $moderationTopics;
	
    /**
     * Bidirectional - One-To-Many (INVERSE SIDE)
     * @ORM\OneToMany(targetEntity="TopicMembership", mappedBy="user", fetch="EXTRA_LAZY", cascade={"persist"})
     */
    protected $registeredTopics;
	
	/**
     * Bidirectional - One-To-Many (INVERSE SIDE)
     * @ORM\OneToMany(targetEntity="PrivateMessage", mappedBy="sender", fetch="EXTRA_LAZY")
     */
    protected $privateMessagesSent;
	
	/**
     * Bidirectional - One-To-Many (INVERSE SIDE)
     * @ORM\OneToMany(targetEntity="PrivateMessage", mappedBy="recipient", fetch="EXTRA_LAZY")
     */
    protected $privateMessagesReceived;
	
    /**
     * Bidirectional - One-To-Many (INVERSE SIDE)
     * @ORM\OneToMany(targetEntity="PrivateMessageBlock", mappedBy="owner", fetch="EXTRA_LAZY")
     */
    protected $messageBlacklist;

    /**
     * Bidirectional - One-To-Many (INVERSE SIDE)
     * @ORM\OneToMany(targetEntity="PrivateMessageBlock", mappedBy="user")
     */
    protected $messageBlacklisted;
	
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return User
     */
    public function setName($name)
    {
		// $name = mb_convert_case($name, MB_CASE_TITLE, 'UTF-8');
		
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return User
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set password
     *
     * @param string $password
     *
     * @return User
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return User
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set about
     *
     * @param string $about
     *
     * @return User
     */
    public function setAbout($about)
    {
        $this->about = $about;

        return $this;
    }

    /**
     * Get about
     *
     * @return string
     */
    public function getAbout()
    {
        return $this->about;
    }

    /**
     * Set languages
     *
     * @param string $languages
     *
     * @return User
     */
    public function setLanguages($languages)
    {
        $this->languages = $languages;

        return $this;
    }

    /**
     * Get languages
     *
     * @return string
     */
    public function getLanguages()
    {
        return $this->languages;
    }

    /**
     * Set registerDate
     *
     * @param \DateTime $registerDate
     *
     * @return User
     */
    public function setRegisterDate($registerDate)
    {
        $this->registerDate = $registerDate;

        return $this;
    }

    /**
     * Get registerDate
     *
     * @return \DateTime
     */
    public function getRegisterDate()
    {
        return $this->registerDate;
    }

    /**
     * Set editDate
     *
     * @param \DateTime $editDate
     *
     * @return User
     */
    public function setEditDate($editDate)
    {
        $this->editDate = $editDate;

        return $this;
    }

    /**
     * Get editDate
     *
     * @return \DateTime
     */
    public function getEditDate()
    {
        return $this->editDate;
    }

    /**
     * Set roles
     *
     * @param array $roles
     *
     * @return User
     */
    public function setRoles($roles)
    {
        $this->roles = $roles;

        return $this;
    }

    public function isAccountNonExpired()
    {
        return true;
    }

    public function isAccountNonLocked()
    {
        return true;
    }

    public function isCredentialsNonExpired()
    {
        return true;
    }

    public function isEnabled()
    {
        return $this->isActive;
    }
    
    /**
     * Get roles
     *
     * @return array
     */
    public function getRoles()
    {
        return $this->roles;
    }
    
    public function getSalt()
    {
        // you *may* need a real salt depending on your encoder
        // see section on salt below
        return null;
    }
    
    /** @see \Serializable::serialize() */
    public function serialize()
    {
        return serialize(array(
            $this->id,
            $this->username,
            $this->email,
            $this->password,
            $this->isActive,
            // see section on salt below
            // $this->salt,
            ));
    }
    /** @see \Serializable::unserialize() */
    public function unserialize($serialized)
    {
        list (
            $this->id,
            $this->username,
            $this->email,
            $this->isActive,
            // see section on salt below
            // $this->salt
        ) = unserialize($serialized);
    }
    
    public function eraseCredentials(){
        
    }
    
    public function getUsername(){
        return $this->username;
    }
    
    public function setUsername($username){
       $this->username = $username;
       
       return $username;
    }

    /**
     * If manually uploading a file (i.e. not using Symfony Form) ensure an instance
     * of 'UploadedFile' is injected into this setter to trigger the  update. If this
     * bundle's configuration parameter 'inject_on_load' is set to 'true' this setter
     * must be able to accept an instance of 'File' as the bundle will inject one here
     * during Doctrine hydration.
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $image
     *
     * @return User
     */
    public function setImageFile(File $image = null)
    {
        $this->imageFile = $image;
        if ($image) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->editDate = new \DateTime('now');
        }

        return $this;
    }

    /**
     * @return File
     */
    public function getImageFile()
    {
        return $this->imageFile;
    }
    
    /**
     * Set avatar
     *
     * @param string $avatar
     *
     * @return User
     */
    public function setAvatar($avatar)
    {
        $this->avatar = $avatar;

        return $this;
    }

    /**
     * Get avatar
     *
     * @return string
     */
    public function getAvatar()
    {
        return $this->avatar;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->threadsAuthored = new \Doctrine\Common\Collections\ArrayCollection();
        $this->threadsUpvoted = new \Doctrine\Common\Collections\ArrayCollection();
        $this->threadsDownvoted = new \Doctrine\Common\Collections\ArrayCollection();
        $this->threadsFollowed = new \Doctrine\Common\Collections\ArrayCollection();
        $this->repliesAuthored = new \Doctrine\Common\Collections\ArrayCollection();
        $this->topicsFollowed= new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add threadsAuthored
     *
     * @param \AppBundle\Entity\Thread $threadsAuthored
     *
     * @return User
     */
    public function addThreadsAuthored(\AppBundle\Entity\Thread $threadsAuthored)
    {
        $this->threadsAuthored[] = $threadsAuthored;

        return $this;
    }

    /**
     * Remove threadsAuthored
     *
     * @param \AppBundle\Entity\Thread $threadsAuthored
     */
    public function removeThreadsAuthored(\AppBundle\Entity\Thread $threadsAuthored)
    {
        $this->threadsAuthored->removeElement($threadsAuthored);
    }

    /**
     * Get threadsAuthored
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getThreadsAuthored()
    {
        return $this->threadsAuthored;
    }

    /**
     * Add usersFollowed
     *
     * @param \AppBundle\Entity\UserFollow $usersFollowed
     *
     * @return User
     */
    public function addUsersFollowed(\AppBundle\Entity\UserFollow $usersFollowed)
    {
        $this->usersFollowed[] = $usersFollowed;

        return $this;
    }

    /**
     * Remove usersFollowed
     *
     * @param \AppBundle\Entity\UserFollow $usersFollowed
     */
    public function removeUsersFollowed(\AppBundle\Entity\UserFollow $usersFollowed)
    {
        $this->usersFollowed->removeElement($usersFollowed);
    }

    /**
     * Get usersFollowed
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUsersFollowed()
    {
        return $this->usersFollowed;
    }

    /**
     * Add follower
     *
     * @param \AppBundle\Entity\UserFollow $follower
     *
     * @return User
     */
    public function addFollower(\AppBundle\Entity\UserFollow $follower)
    {
        $this->followers[] = $follower;

        return $this;
    }

    /**
     * Remove follower
     *
     * @param \AppBundle\Entity\UserFollow $follower
     */
    public function removeFollower(\AppBundle\Entity\UserFollow $follower)
    {
        $this->followers->removeElement($follower);
    }

    /**
     * Get followers
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFollowers()
    {
        return $this->followers;
    }

    /**
     * Add topicsFollowed
     *
     * @param \AppBundle\Entity\TopicFollow $topicsFollowed
     *
     * @return User
     */
    public function addTopicsFollowed(\AppBundle\Entity\TopicFollow $topicsFollowed)
    {
        $this->topicsFollowed[] = $topicsFollowed;

        return $this;
    }

    /**
     * Remove topicsFollowed
     *
     * @param \AppBundle\Entity\TopicFollow $topicsFollowed
     */
    public function removeTopicsFollowed(\AppBundle\Entity\TopicFollow $topicsFollowed)
    {
        $this->topicsFollowed->removeElement($topicsFollowed);
    }

    /**
     * Get topicsFollowed
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTopicsFollowed()
    {
        return $this->topicsFollowed;
    }

    /**
     * Add repliesAuthored
     *
     * @param \AppBundle\Entity\Reply $repliesAuthored
     *
     * @return User
     */
    public function addRepliesAuthored(\AppBundle\Entity\Reply $repliesAuthored)
    {
        $this->repliesAuthored[] = $repliesAuthored;

        return $this;
    }

    /**
     * Remove repliesAuthored
     *
     * @param \AppBundle\Entity\Reply $repliesAuthored
     */
    public function removeRepliesAuthored(\AppBundle\Entity\Reply $repliesAuthored)
    {
        $this->repliesAuthored->removeElement($repliesAuthored);
    }

    /**
     * Get repliesAuthored
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRepliesAuthored()
    {
        return $this->repliesAuthored;
    }

    /**
     * Add threadsFollowed
     *
     * @param \AppBundle\Entity\ThreadFollow $threadsFollowed
     *
     * @return User
     */
    public function addThreadsFollowed(\AppBundle\Entity\ThreadFollow $threadsFollowed)
    {
        $this->threadsFollowed[] = $threadsFollowed;

        return $this;
    }

    /**
     * Remove threadsFollowed
     *
     * @param \AppBundle\Entity\ThreadFollow $threadsFollowed
     */
    public function removeThreadsFollowed(\AppBundle\Entity\ThreadFollow $threadsFollowed)
    {
        $this->threadsFollowed->removeElement($threadsFollowed);
    }

    /**
     * Get threadsFollowed
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getThreadsFollowed()
    {
        return $this->threadsFollowed;
    }

    /**
     * Set isActive
     *
     * @param boolean $isActive
     *
     * @return User
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * Get isActive
     *
     * @return boolean
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * Set activationCode
     *
     * @return User
     */
    public function setActivationCode()
    {
        $activationCode = bin2hex(mcrypt_create_iv(16, MCRYPT_DEV_URANDOM));
        
        $this->activationCode = $activationCode;

        return $this;
    }

    /**
     * Get activationCode
     *
     * @return string
     */
    public function getActivationCode()
    {
        return $this->activationCode;
    }

    /**
     * Add title
     *
     * @param \AppBundle\Entity\TopicTitle $title
     *
     * @return User
     */
    public function addTitle(\AppBundle\Entity\TopicTitle $title)
    {
        $this->titles[] = $title;

        return $this;
    }

    /**
     * Remove title
     *
     * @param \AppBundle\Entity\TopicTitle $title
     */
    public function removeTitle(\AppBundle\Entity\TopicTitle $title)
    {
        $this->titles->removeElement($title);
    }

    /**
     * Get titles
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTitles()
    {
        return $this->titles;
    }

    /**
     * Add threadsUpvoted
     *
     * @param \AppBundle\Entity\ThreadUpvote $threadsUpvoted
     *
     * @return User
     */
    public function addThreadsUpvoted(\AppBundle\Entity\ThreadUpvote $threadsUpvoted)
    {
        $this->threadsUpvoted[] = $threadsUpvoted;

        return $this;
    }

    /**
     * Remove threadsUpvoted
     *
     * @param \AppBundle\Entity\ThreadUpvote $threadsUpvoted
     */
    public function removeThreadsUpvoted(\AppBundle\Entity\ThreadUpvote $threadsUpvoted)
    {
        $this->threadsUpvoted->removeElement($threadsUpvoted);
    }

    /**
     * Get threadsUpvoted
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getThreadsUpvoted()
    {
        return $this->threadsUpvoted;
    }

    /**
     * Add threadsDownvoted
     *
     * @param \AppBundle\Entity\ThreadDownvote $threadsDownvoted
     *
     * @return User
     */
    public function addThreadsDownvoted(\AppBundle\Entity\ThreadDownvote $threadsDownvoted)
    {
        $this->threadsDownvoted[] = $threadsDownvoted;

        return $this;
    }

    /**
     * Remove threadsDownvoted
     *
     * @param \AppBundle\Entity\ThreadDownvote $threadsDownvoted
     */
    public function removeThreadsDownvoted(\AppBundle\Entity\ThreadDownvote $threadsDownvoted)
    {
        $this->threadsDownvoted->removeElement($threadsDownvoted);
    }

    /**
     * Get threadsDownvoted
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getThreadsDownvoted()
    {
        return $this->threadsDownvoted;
    }

    /**
     * Add repliesUpvoted
     *
     * @param \AppBundle\Entity\ReplyUpvote $repliesUpvoted
     *
     * @return User
     */
    public function addRepliesUpvoted(\AppBundle\Entity\ReplyUpvote $repliesUpvoted)
    {
        $this->repliesUpvoted[] = $repliesUpvoted;

        return $this;
    }

    /**
     * Remove repliesUpvoted
     *
     * @param \AppBundle\Entity\ReplyUpvote $repliesUpvoted
     */
    public function removeRepliesUpvoted(\AppBundle\Entity\ReplyUpvote $repliesUpvoted)
    {
        $this->repliesUpvoted->removeElement($repliesUpvoted);
    }

    /**
     * Get repliesUpvoted
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRepliesUpvoted()
    {
        return $this->repliesUpvoted;
    }

    /**
     * Add repliesDownvoted
     *
     * @param \AppBundle\Entity\ReplyDownvote $repliesDownvoted
     *
     * @return User
     */
    public function addRepliesDownvoted(\AppBundle\Entity\ReplyDownvote $repliesDownvoted)
    {
        $this->repliesDownvoted[] = $repliesDownvoted;

        return $this;
    }

    /**
     * Remove repliesDownvoted
     *
     * @param \AppBundle\Entity\ReplyDownvote $repliesDownvoted
     */
    public function removeRepliesDownvoted(\AppBundle\Entity\ReplyDownvote $repliesDownvoted)
    {
        $this->repliesDownvoted->removeElement($repliesDownvoted);
    }

    /**
     * Get repliesDownvoted
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRepliesDownvoted()
    {
        return $this->repliesDownvoted;
    }

    /**
     * Add threadDraftsAuthored
     *
     * @param \AppBundle\Entity\ThreadDraft $threadDraftsAuthored
     *
     * @return User
     */
    public function addThreadDraftsAuthored(\AppBundle\Entity\ThreadDraft $threadDraftsAuthored)
    {
        $this->threadDraftsAuthored[] = $threadDraftsAuthored;

        return $this;
    }

    /**
     * Remove threadDraftsAuthored
     *
     * @param \AppBundle\Entity\ThreadDraft $threadDraftsAuthored
     */
    public function removeThreadDraftsAuthored(\AppBundle\Entity\ThreadDraft $threadDraftsAuthored)
    {
        $this->threadDraftsAuthored->removeElement($threadDraftsAuthored);
    }

    /**
     * Get threadDraftsAuthored
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getThreadDraftsAuthored()
    {
        return $this->threadDraftsAuthored;
    }

    /**
     * Add replyDraftsAuthored
     *
     * @param \AppBundle\Entity\ReplyDraft $replyDraftsAuthored
     *
     * @return User
     */
    public function addReplyDraftsAuthored(\AppBundle\Entity\ReplyDraft $replyDraftsAuthored)
    {
        $this->replyDraftsAuthored[] = $replyDraftsAuthored;

        return $this;
    }

    /**
     * Remove replyDraftsAuthored
     *
     * @param \AppBundle\Entity\ReplyDraft $replyDraftsAuthored
     */
    public function removeReplyDraftsAuthored(\AppBundle\Entity\ReplyDraft $replyDraftsAuthored)
    {
        $this->replyDraftsAuthored->removeElement($replyDraftsAuthored);
    }

    /**
     * Get replyDraftsAuthored
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getReplyDraftsAuthored()
    {
        return $this->replyDraftsAuthored;
    }

    /**
     * Add moderationTopic
     *
     * @param \AppBundle\Entity\RoleForTopic $moderationTopic
     *
     * @return User
     */
    public function addModerationTopic(\AppBundle\Entity\RoleForTopic $moderationTopic)
    {
        $this->moderationTopics[] = $moderationTopic;

        return $this;
    }

    /**
     * Remove moderationTopic
     *
     * @param \AppBundle\Entity\RoleForTopic $moderationTopic
     */
    public function removeModerationTopic(\AppBundle\Entity\RoleForTopic $moderationTopic)
    {
        $this->moderationTopics->removeElement($moderationTopic);
    }

    /**
     * Get moderationTopics
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getModerationTopics()
    {
        return $this->moderationTopics;
    }

    /**
     * Add sentbox
     *
     * @param \AppBundle\Entity\PrivateMessage $sentbox
     *
     * @return User
     */
    public function addSentbox(\AppBundle\Entity\PrivateMessage $sentbox)
    {
        $this->sentbox[] = $sentbox;

        return $this;
    }

    /**
     * Remove sentbox
     *
     * @param \AppBundle\Entity\PrivateMessage $sentbox
     */
    public function removeSentbox(\AppBundle\Entity\PrivateMessage $sentbox)
    {
        $this->sentbox->removeElement($sentbox);
    }

    /**
     * Add privateMessagesSent
     *
     * @param \AppBundle\Entity\PrivateMessage $privateMessagesSent
     *
     * @return User
     */
    public function addPrivateMessagesSent(\AppBundle\Entity\PrivateMessage $privateMessagesSent)
    {
        $this->privateMessagesSent[] = $privateMessagesSent;

        return $this;
    }

    /**
     * Remove privateMessagesSent
     *
     * @param \AppBundle\Entity\PrivateMessage $privateMessagesSent
     */
    public function removePrivateMessagesSent(\AppBundle\Entity\PrivateMessage $privateMessagesSent)
    {
        $this->privateMessagesSent->removeElement($privateMessagesSent);
    }

    /**
     * Get privateMessagesSent
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPrivateMessagesSent()
    {
        return $this->privateMessagesSent;
    }

    /**
     * Add privateMessagesReceived
     *
     * @param \AppBundle\Entity\PrivateMessage $privateMessagesReceived
     *
     * @return User
     */
    public function addPrivateMessagesReceived(\AppBundle\Entity\PrivateMessage $privateMessagesReceived)
    {
        $this->privateMessagesReceived[] = $privateMessagesReceived;

        return $this;
    }

    /**
     * Remove privateMessagesReceived
     *
     * @param \AppBundle\Entity\PrivateMessage $privateMessagesReceived
     */
    public function removePrivateMessagesReceived(\AppBundle\Entity\PrivateMessage $privateMessagesReceived)
    {
        $this->privateMessagesReceived->removeElement($privateMessagesReceived);
    }

    /**
     * Get privateMessagesReceived
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPrivateMessagesReceived()
    {
        return $this->privateMessagesReceived;
    }

    /**
     * Add messageBlacklist
     *
     * @param \AppBundle\Entity\PrivateMessageBlock $messageBlacklist
     *
     * @return User
     */
    public function addMessageBlacklist(\AppBundle\Entity\PrivateMessageBlock $messageBlacklist)
    {
        $this->messageBlacklist[] = $messageBlacklist;

        return $this;
    }

    /**
     * Remove messageBlacklist
     *
     * @param \AppBundle\Entity\PrivateMessageBlock $messageBlacklist
     */
    public function removeMessageBlacklist(\AppBundle\Entity\PrivateMessageBlock $messageBlacklist)
    {
        $this->messageBlacklist->removeElement($messageBlacklist);
    }

    /**
     * Get messageBlacklist
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMessageBlacklist()
    {
        return $this->messageBlacklist;
    }

    /**
     * Add messageBlacklisted
     *
     * @param \AppBundle\Entity\PrivateMessageBlock $messageBlacklisted
     *
     * @return User
     */
    public function addMessageBlacklisted(\AppBundle\Entity\PrivateMessageBlock $messageBlacklisted)
    {
        $this->messageBlacklisted[] = $messageBlacklisted;

        return $this;
    }

    /**
     * Remove messageBlacklisted
     *
     * @param \AppBundle\Entity\PrivateMessageBlock $messageBlacklisted
     */
    public function removeMessageBlacklisted(\AppBundle\Entity\PrivateMessageBlock $messageBlacklisted)
    {
        $this->messageBlacklisted->removeElement($messageBlacklisted);
    }

    /**
     * Get messageBlacklisted
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMessageBlacklisted()
    {
        return $this->messageBlacklisted;
    }

    /**
     * Set acceptMessages
     *
     * @param string $acceptMessages
     *
     * @return User
     */
    public function setAcceptMessages($acceptMessages = 'open')
    {
        $this->acceptMessages = $acceptMessages;

        return $this;
    }

    /**
     * Get acceptMessages
     *
     * @return string
     */
    public function getAcceptMessages()
    {
        return $this->acceptMessages;
    }

    /**
     * Set isBanned
     *
     * @param boolean $isBanned
     *
     * @return User
     */
    public function setIsBanned($isBanned)
    {
        $this->isBanned = $isBanned;

        return $this;
    }

    /**
     * Get isBanned
     *
     * @return boolean
     */
    public function getIsBanned()
    {
        return $this->isBanned;
    }

    /**
     * Set banReason
     *
     * @param string $banReason
     *
     * @return User
     */
    public function setBanReason($banReason)
    {
        $this->banReason = $banReason;

        return $this;
    }

    /**
     * Get banReason
     *
     * @return string
     */
    public function getBanReason()
    {
        return $this->banReason;
    }

    /**
     * Add threadVisit
     *
     * @param \AppBundle\Entity\ThreadView $threadVisit
     *
     * @return User
     */
    public function addThreadVisit(\AppBundle\Entity\ThreadView $threadVisit)
    {
        $this->threadVisits[] = $threadVisit;

        return $this;
    }

    /**
     * Remove threadVisit
     *
     * @param \AppBundle\Entity\ThreadView $threadVisit
     */
    public function removeThreadVisit(\AppBundle\Entity\ThreadView $threadVisit)
    {
        $this->threadVisits->removeElement($threadVisit);
    }

    /**
     * Get threadVisits
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getThreadVisits()
    {
        return $this->threadVisits;
    }

    /**
     * Set browserLocale
     *
     * @param string $browserLocale
     *
     * @return User
     */
    public function setBrowserLocale($browserLocale)
    {			
		if(strpos($browserLocale, '-') !== false){
			$l = strtolower(explode('-', $browserLocale)[0]);
		}elseif(strpos($browserLocale, '_') !== false){
			$l = strtolower(explode('_', $browserLocale)[0]);
		}else{
			$l = strtolower($browserLocale);
		}
		
        $this->browserLocale = $l;

        return $this;
    }

    /**
     * Get browserLocale
     *
     * @return string
     */
    public function getBrowserLocale()
    {
        return $this->browserLocale;
    }

    /**
     * Add registeredTopic
     *
     * @param \AppBundle\Entity\TopicMembership $registeredTopic
     *
     * @return User
     */
    public function addRegisteredTopic(\AppBundle\Entity\TopicMembership $registeredTopic)
    {
        $this->registeredTopics[] = $registeredTopic;

        return $this;
    }

    /**
     * Remove registeredTopic
     *
     * @param \AppBundle\Entity\TopicMembership $registeredTopic
     */
    public function removeRegisteredTopic(\AppBundle\Entity\TopicMembership $registeredTopic)
    {
        $this->registeredTopics->removeElement($registeredTopic);
    }

    /**
     * Get registeredTopics
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRegisteredTopics()
    {
        return $this->registeredTopics;
    }
}

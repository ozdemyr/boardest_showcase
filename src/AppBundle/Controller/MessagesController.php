<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use AppBundle\Entity\PrivateMessage;

/**
 * @Security("has_role('ROLE_USER')")
 */
class MessagesController extends Controller
{
	/**
	 * @Route("messages", name="messages", defaults={"locale": "en"}, host="{_locale}.boardest.com", requirements={
     *     "_locale": "%app.locales%"
     * })
	 * @Security("has_role('ROLE_USER')")
	 */
	public function conversationsAction(Request $request)
	{
		$em = $this->getDoctrine()->getManager();
		
		$conversations = $em->getRepository('AppBundle\Entity\PrivateMessage')
							->getConversations($this->getUser());
						
		$mobileDetector = $this->get('mobile_detect.mobile_detector');
        if($mobileDetector->isMobile()){
			return $this->render('default/mobile/messages.html.twig', array(
				'conversations' => $conversations
				));
		}else{
			return $this->render('default/messages.html.twig', array(
				'conversations' => $conversations
				));
		}
	}
	
	
	/**
	 * @Route("messages/new", name="messages_new", defaults={"locale": "en"}, host="{_locale}.boardest.com", requirements={
     *     "_locale": "%app.locales%"
     * })
	 * @Security("has_role('ROLE_USER')")
	 */
	public function newAction(Request $request)
	{
		$em = $this->getDoctrine()->getManager();
		
		if($request->query->get('s')){
			$dql = "SELECT u.id, u.username, u.name, u.avatar, 'user' as type FROM AppBundle\Entity\User u WHERE (u.name LIKE ?1 OR u.username LIKE ?1) AND u.id != :appuser AND u.isActive = 1";
			$results = $em->createQuery($dql)
							->setParameter(':appuser', $this->getUser())
							->setMaxResults(5)
							->setParameter(1, '%' . $request->query->get('s') . '%')
							->getResult();
							
			$response = new Response();
			$response->setContent(json_encode(array(
				'status' => 1,
				'render' => $this->renderView('default/ajax/search-widget-results-for-messages-new.html.twig', array(
					'results' => $results,
					's' => $request->query->get('s')
				))
			)));
			$response->headers->set('Content-Type', 'application/json');

			return $response;
		}
		
					
		$mobileDetector = $this->get('mobile_detect.mobile_detector');
        if($mobileDetector->isMobile()){
			return $this->render('default/mobile/messages-new.html.twig', array(
				));
		}else{
			// Konu�malar G�r�ld� yap�lmas�n diye mobilde �ekmiyoruz
			$conversations = $em->getRepository('AppBundle\Entity\PrivateMessage')
							->getConversations($this->getUser());
							
			return $this->render('default/messages-new.html.twig', array(
				'conversations' => $conversations
				));
		}
	}
	
	/**
	 * @Route("messages/with/{username}", name="conversation", defaults={"locale": "en"}, host="{_locale}.boardest.com", requirements={
     *     "_locale": "%app.locales%"
     * })
	 * @Security("has_role('ROLE_USER')")
	 */
	public function conversationAction(Request $request, $username)
	{
		$em = $this->getDoctrine()->getManager();
		$hashids = $this->container->get('app.hashids');
		
		
		$dql = "SELECT partial u.{id, name, username, title, avatar, acceptMessages}
		FROM AppBundle\Entity\User u
		WHERE u.username = :username";
		
		$user = $em->createQuery($dql)
					->setParameter(':username', $username)
					->getOneOrNullResult();
		
		if(!$user){
			return $this->redirectToRoute('messages');
		}
		
		$user->isBlocked = $em->getRepository('AppBundle\Entity\User')->isBlocked($this->getUser(), $user);
		$user->hasBlocked = $em->getRepository('AppBundle\Entity\User')->isBlocked($user, $this->getUser());
		$user->hasFollowed = $em->getRepository('AppBundle\Entity\User')->isFollowed($user, $this->getUser());
		
		if(!$user){
			return $this->redirectToRoute('messages');
		}elseif($user->getUsername() == $this->getUser()->getUsername()){
			return $this->redirectToRoute('messages');			
		}
		
		if($request->getMethod() == 'POST') {
			
			switch($user->getAcceptMessages()){
				case 'open':
					if($user->hasBlocked){
						$response = new Response();
						$response->setContent(json_encode(array(
							'status' => 0,
							'exception' => 'blocked',
						)));		
						$response->headers->set('Content-Type', 'application/json');

						return $response;
					}
				default:
				break;
				
				case 'closed':
					$response = new Response();
					$response->setContent(json_encode(array(
						'status' => 0,
						'exception' => 'closed',
					)));		
					$response->headers->set('Content-Type', 'application/json');

					return $response;
				break;
				
				case 'usersfollowed':
					if(!$user->hasFollowed){
						$response = new Response();
						$response->setContent(json_encode(array(
							'status' => 0,
							'exception' => 'usersfollowed',
						)));		
						$response->headers->set('Content-Type', 'application/json');

						return $response;
					}elseif($user->hasBlocked){
						$response = new Response();
						$response->setContent(json_encode(array(
							'status' => 0,
							'exception' => 'blocked',
						)));		
						$response->headers->set('Content-Type', 'application/json');

						return $response;
					}
				break;
			}
			
			$message = new PrivateMessage();
			
			$message->setSender($this->getUser());
			$message->setRecipient($user);
			$message->setContent($request->request->get('content'));
			$message->setStatus('open');
			$message->setAddDate(new \DateTime('now'));
			$message->setIsSeen(0);
			$message->setIsRead(0);
			$message->setIsNotificated(0);
			
			$validator = $this->get('validator');
			$errors = $validator->validate($message);
			
			if (count($errors) > 0) {
				$response = new Response();
				$response->setContent(json_encode(array(
					'status' => 0,
					'exception' => (string) $errors,
				)));		
				$response->headers->set('Content-Type', 'application/json');

				return $response;		
			}
			
			try{
				$em->persist($message);
				$em->flush();
				
				$conversation = $em->getRepository('AppBundle\Entity\PrivateMessage')
								   ->getConversationUnit($this->getUser(), $user);
				
				$response = new Response();
				$response->setContent(json_encode(array(
					'status' => 1,
					'render' => array(
						'message' => $this->renderView('default/snippets/message-unit.html.twig', array(
								'message' => $message,
								'mimics' => array('new', 'open')
							)),
						'conversation' => $this->renderView('default/snippets/conversation-unit.html.twig', array(
								'user' => $user,
								'conversation' => $conversation,
								'mimics' => array('new', 'focused')
							)),
						),
					'targets' => array(
						'message_id' => $hashids->encode($message->getId()),
						'conversation_id' => $hashids->encode($conversation['user']->getId())
					)
				)));		
				$response->headers->set('Content-Type', 'application/json');

				return $response;
			}catch(Exception $ex){
				$response = new Response();
				$response->setContent(json_encode(array(
					'status' => 0,
					'exception' => $ex,
				)));		
				$response->headers->set('Content-Type', 'application/json');

				return $response;				
			}
		}
							
		$em->getRepository('AppBundle\Entity\PrivateMessage')
							->setConversationAsSeen($this->getUser(), $user);
		
		$conversation = $em->getRepository('AppBundle\Entity\PrivateMessage')
							->getConversation($this->getUser(), $user);
		
		// $paginator  = $this->get('knp_paginator');
        // $conversation = $paginator->paginate(
            // $q,
            // $request->query->getInt('page', 1), 10
        // );
			

		$mobileDetector = $this->get('mobile_detect.mobile_detector');
        if($mobileDetector->isMobile()){
			return $this->render('default/mobile/messages-conversation.html.twig', array(
				'user' => $user,
				'conversation' => $conversation
				));
		}else{
			// Konu�malar G�r�ld� yap�lmas�n diye mobilde �ekmiyoruz
			$conversations = $em->getRepository('AppBundle\Entity\PrivateMessage')
							->getConversations($this->getUser());
							
			return $this->render('default/messages-conversation.html.twig', array(
				'conversations' => $conversations,
				'user' => $user,
				'conversation' => $conversation
				));
		}
	}

/**
	 * @Route("messages/with/{username}/feed", name="conversation_feed", defaults={"locale": "en"}, host="{_locale}.boardest.com", requirements={
     *     "_locale": "%app.locales%"
     * })
	 * @Security("has_role('ROLE_USER')")
	 */
	public function conversationFeedAction(Request $request, $username)
	{
		$em = $this->getDoctrine()->getManager();
		$hashids = $this->container->get('app.hashids');
		
		$dql = "SELECT partial u.{id, name, username, title, avatar}
		FROM AppBundle\Entity\User u
		WHERE u.username = :username";
		
		$user = $em->createQuery($dql)
					->setParameter(':username', $username)
					->getOneOrNullResult();
			
		if(!$user){
			$response = new Response();
			$response->setContent(json_encode(array(
				'status' => 0,
				'exception' => 'not-found'
			)));
			$response->headers->set('Content-Type', 'application/json');

			return $response;
		}
		
		$em->getRepository('AppBundle\Entity\PrivateMessage')
							->setConversationAsSeen($this->getUser(), $user);
		
		$conversation = $em->getRepository('AppBundle\Entity\PrivateMessage')
							->getConversation($this->getUser(), $user, $request->query->get('offset'));
							
		$paginator  = $this->get('knp_paginator');
		// $conversation = $paginator->paginate(
			// $q,
			// $request->query->getInt('page', 1), 10,
			// array('offset_shift' => $request->query->get('appendedMessageCount'))
		// );
		
		if($conversation){
			$render = '';
			foreach($conversation as $messageUnit){
				$render .= $this->renderView('default/snippets/message-unit.html.twig', array(
					'message' => $messageUnit['message'],
					'mimics' => array('new', 'folded')
				));
			}
			
			$response = new Response();
			$response->setContent(json_encode(array(
				'status' => 1,
				'render' => $render
			)));
			$response->headers->set('Content-Type', 'application/json');

			return $response;
		}else{
			$response = new Response();
			$response->setContent(json_encode(array(
				'status' => 0
			)));
			$response->headers->set('Content-Type', 'application/json');

			return $response;
		}
		
		$response = new Response();
		$response->setContent(json_encode(array(
			'status' => 0
		)));
		$response->headers->set('Content-Type', 'application/json');

		return $response;
	}	
		
	/**
	 * @Route("conversation/delete", name="conversation_delete", host="{_locale}.boardest.com", requirements={
     *     "_locale": "%app.locales%"
     * })
	 * @Security("has_role('ROLE_USER')")
	 */
	public function deleteConversationAction(Request $request)
	{
		if($request->getMethod() == 'POST') {
			
			$em = $this->getDoctrine()->getManager();
			$hashids = $this->container->get('app.hashids');
			
			$user = $hashids->decode($request->query->get('conversation_with'))[0];
			
			try{
				// Kal�c� olarak sil
				// Al�c� taraf�ndan silinmi� olanlar� kal�c� olarak sil
				$dql = "DELETE FROM AppBundle\Entity\PrivateMessage pm
				WHERE pm.sender = :appuser 
				AND pm.recipient = :user 
				AND pm.status = 'deleted-by-recipient'";
				
				$em->createQuery($dql)
				   ->setParameter(':appuser', $this->getUser())
				   ->setParameter(':user', $user)
				   ->execute();
				   
				// G�nderici taraf�ndan silinmi� olanlar� kal�c� olarak sil
				$dql = "DELETE FROM AppBundle\Entity\PrivateMessage pm
				WHERE pm.sender = :user
				AND pm.recipient = :appuser 
				AND pm.status = 'deleted-by-sender'";
				
				$em->createQuery($dql)
				   ->setParameter(':appuser', $this->getUser())
				   ->setParameter(':user', $user)
				   ->execute();
				
				// �stemci taraf�ndan sil
				// G�nderi isek open olanlar� al�c� i�in silinmi� yap
				$dql = "UPDATE AppBundle\Entity\PrivateMessage pm
				SET pm.status = 'deleted-by-sender'
				WHERE pm.sender = :appuser 
				AND pm.recipient = :user";
			
				$em->createQuery($dql)
				   ->setParameter(':appuser', $this->getUser())
				   ->setParameter(':user', $user)
				   ->execute();
				   
				// Al�c� isek open olanlar� al�c� i�in silinmi� yap
				$dql = "UPDATE AppBundle\Entity\PrivateMessage pm
				SET pm.status = 'deleted-by-recipient'
				WHERE pm.sender = :user
				AND pm.recipient = :appuser";
				
				$em->createQuery($dql)
				   ->setParameter(':appuser', $this->getUser())
				   ->setParameter(':user', $user)
				   ->execute();
				   
				$response = new Response();
				$response->setContent(json_encode(array(
					'status' => 1,
					'target_id' => $hashids->encode($user)
				)));
				$response->headers->set('Content-Type', 'application/json');

				return $response;
				
			}catch(Exception $ex){
				$response = new Response();
				$response->setContent(json_encode(array(
					'status' => 0,
					'exception' => $ex
				)));
				$response->headers->set('Content-Type', 'application/json');

				return $response;
			}
		}
		   
		return $this->render('default/ajax/confirm-delete-conversation.html.twig', array(
            'conversation_with' => $request->query->get('conversation_with'),
        ));
	}
	
	/**
	 * @Route("messages/delete", name="message_delete", host="{_locale}.boardest.com", requirements={
     *     "_locale": "%app.locales%"
     * })
	 * @Security("has_role('ROLE_USER')")
	 */
	public function deleteMessageAction(Request $request)
	{
		$em = $this->getDoctrine()->getManager();
		$hashids = $this->container->get('app.hashids');
		
		$message_id = $hashids->decode($request->query->get('message_id'))[0];
		
		$dql = "SELECT pm as message,
		partial pms.{id, name, username, title, avatar},
		partial pmr.{id, name, username, title, avatar},
		'sender' AS sendType
		FROM AppBundle\Entity\PrivateMessage pm
		LEFT JOIN pm.sender pms
		LEFT JOIN pm.recipient pmr
		WHERE pm.id = :pm
		AND pm.sender = :appuser
		AND pm.status NOT IN ('deleted', 'deleted-by-sender')";
		
		$asSender = $em->createQuery($dql)
					->setParameter(':pm', $message_id)
					->setParameter(':appuser', $this->getUser())
					->getOneOrNullResult();
							
		$dql = "SELECT pm as message,
		partial pms.{id, name, username, title, avatar},
		partial pmr.{id, name, username, title, avatar},
		'recipient' AS sendType
		FROM AppBundle\Entity\PrivateMessage pm
		LEFT JOIN pm.sender pms
		LEFT JOIN pm.recipient pmr
		WHERE pm.id = :pm
		AND pm.recipient = :appuser
		AND pm.status NOT IN ('deleted', 'deleted-by-recipient')";
		
		$asRecipient = $em->createQuery($dql)
					->setParameter(':pm', $message_id)
					->setParameter(':appuser', $this->getUser())
					->getOneOrNullResult();
		
		// Belli bir ID'si olan mesaj ya al�c� olarak g�nderilmi�tir ya da g�nderici. E�er b�yle bir mesaj yoksa bulunamad� hatas� ver.
		$messageUnit = $asSender ? $asSender : $asRecipient;
		$conversationWith = $messageUnit['sendType'] == 'sender' ? $messageUnit['message']->getRecipient() : $messageUnit['message']->getSender();
		
		if(!$asSender && !$asRecipient){
			$response = new Response();
            $response->setContent(json_encode(array(
                'status' => 0,
                'exception' => 'not-found'
            )));
            $response->headers->set('Content-Type', 'application/json');

            return $response;
		}
		
		if($request->getMethod() == 'POST') {
			
			try{
				switch($messageUnit['message']->getStatus()){
					// E�er kimse taraf�ndan silinmemi�se, silen taraf�ndan silinmi� hale getir
					case 'open':
					default:
					
						if($messageUnit['sendType'] == 'sender'){
							$messageUnit['message']->setStatus('deleted-by-sender');
						}elseif($messageUnit['sendType'] == 'recipient'){
							$messageUnit['message']->setStatus('deleted-by-recipient');					
						}
						
						$em->persist($messageUnit['message']);
						$em->flush();
						
					break;
					
					// Daha �nce Sender taraf�ndan silinmi�se ve �u an silen Recipient'sa kal�c� olarak sil veya 'deleted' haline getir
					case 'deleted-by-sender':
						if($messageUnit['sendType'] == 'recipient'){
							$dql = "DELETE FROM AppBundle\Entity\PrivateMessage pm WHERE pm.id = :message";
							$em->createQuery($dql)
							   ->setParameter(':message', $messageUnit['message'])
							   ->execute();
						}
					break;
					
					// Daha �nce Recipient taraf�ndan silinmi�se ve �u an silen Sender'sa kal�c� olarak sil veya 'deleted' haline getir
					case 'deleted-by-recipient':
						if($messageUnit['sendType'] == 'sender'){
							$dql = "DELETE FROM AppBundle\Entity\PrivateMessage pm WHERE pm.id = :message";
							$em->createQuery($dql)
							   ->setParameter(':message', $messageUnit['message'])
							   ->execute();
						}					
					break;
					
					// Zaten silinmi�se
					case 'deleted':
					break;
				}
				
				$conversation = $em->getRepository('AppBundle\Entity\PrivateMessage')
								   ->getConversationUnit($this->getUser(), $conversationWith);
				
				$renderConversation = $conversation ? $this->renderView('default/snippets/conversation-unit.html.twig', array(
							'conversation' => $conversation,
							'user' => $conversationWith,
							'mimics' => array('focused')
						)) : false;
				
				$response = new Response();
				$response->setContent(json_encode(array(
					'status' => 1,
					'targets' => array(
						'message_id' => $hashids->encode($messageUnit['message']->getId()),
						'conversation_id' => $hashids->encode($conversationWith->getId()),
						),
					'render' => array(
						'conversation' => $renderConversation
					)
				)));
				$response->headers->set('Content-Type', 'application/json');
				
				return $response;
				
			}catch(Exception $ex){
				$response = new Response();
				$response->setContent(json_encode(array(
					'status' => 0,
					'exception' => $ex
				)));
				$response->headers->set('Content-Type', 'application/json');

				return $response;
			}
		}

		return $this->render('default/ajax/confirm-delete-message.html.twig', array(
            'message_id' => $request->query->get('message_id'),
        ));
	}
	
	/**
     * @Route("messages/block", name="messages_block", condition="request.isXmlHttpRequest()", host="{_locale}.%base_host%", requirements={
     *     "_locale": "%app.locales%"
     * })
     */
    public function blockPrivateMessagesAction(Request $request)
    {
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            $response = new Response();
            $response->setContent(json_encode(array(
                'status' => 0,
                'exception' => 'loggedout'
            )));
            $response->headers->set('Content-Type', 'application/json');

            return $response;
        }
        
        if($request->getMethod() == 'POST') {
            
            $em = $this->getDoctrine()->getManager();
            $hashids = $this->container->get('app.hashids');
			
			$user_id = $hashids->decode($request->request->get('user_id'))[0];
			
			$user = $em->getRepository('AppBundle\Entity\User')->findOneBy(array(
				'id' => $user_id
			));
			
            // E�er kendisini takip etmek istiyorsa geri d�nd�r
            if($request->request->get('user_id') == $this->getUser()->getId()){
                $response = new Response();
                $response->setContent(json_encode(array(
                    'status' => 0,
                    'exception' => 'own'
                )));
                $response->headers->set('Content-Type', 'application/json');

                return $response;
            }
            
            try {
				$action = $em->getRepository('AppBundle\Entity\User')
				->togglePrivateMessageBlock($this->getUser(), $user);
				
				$response = new Response();
                $response->setContent(json_encode(array(
                    'status' => 1,
                    'action' => $action,
					'target_id' => $hashids->encode($user->getId())
                )));
                $response->headers->set('Content-Type', 'application/json');

                return $response;
				
            } catch (Exception $ex) {
                $response = new Response();
                $response->setContent(json_encode(array(
                    'status' => 0,
                    'errors' => $ex
                )));
                $response->headers->set('Content-Type', 'application/json');

                return $response;
            }
        }
        
        return null;
    }
}
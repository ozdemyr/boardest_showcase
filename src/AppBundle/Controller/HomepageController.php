<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\ORM\Query;

class HomepageController extends Controller
{
    /**
     * @Route("/{tab}", name="homepage_redirect", defaults={"tab": "all"}, host="%base_host%", requirements={
     *     "tab": "all|questions|links|threads"
     * })
     */
    public function redirectAction(Request $request, $tab)
    {
		if(isset($_SERVER['HTTP_ACCEPT_LANGUAGE']) && $_SERVER['HTTP_ACCEPT_LANGUAGE']){
			if(strpos($_SERVER['HTTP_ACCEPT_LANGUAGE'], '-') !== false){
				$l = strtolower(explode('-', $_SERVER['HTTP_ACCEPT_LANGUAGE'])[0]);
			}else{
				$l = strtolower($_SERVER['HTTP_ACCEPT_LANGUAGE']);
			}
			
			// sonra array'daki dilleri configden otomatik çek şimdilik iyi
			if(in_array($l, $this->container->getParameter('app.public_locales'))){
				$request->setLocale($l);
			}
		}
		
		return $this->redirectToRoute('homepage', array(
			'tab' => $tab,
			'_locale' => $request->getLocale()
		));
	}
	
    /**
     * @Route("/{tab}", name="homepage", defaults={"tab": "all"}, host="{_locale}.%base_host%", requirements={
     *     "tab": "all|questions|links|threads",
     *     "_locale": "%app.locales%"
     * })
     */
    public function indexAction(Request $request, $tab)
    {

        $em = $this->getDoctrine()->getManager();
            
        // Eğer oturum açık değilse
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            // Thread'leri getir
            $q = $em->getRepository('AppBundle\Entity\Thread')
                    ->getThreads(array(
						'appuser' => $this->getUser(),
                        'type' => $tab,
                        'locale' => $request->getLocale(),
						'env' => $this->container->get('kernel')->getEnvironment(),
						'sorting' => $request->query->get('sorting'),
						'date' => $request->query->get('date')
                    ));

            $paginator  = $this->get('knp_paginator');
            $threadUnits = $paginator->paginate(
                $q,
                $request->query->getInt('page', 1), 10
            );
			
			// Yorum baloncuklarını getir
			if($threadUnits->items){
				foreach($threadUnits->items as &$threadUnit){
					$dql = "SELECT partial u.{id, name, username, avatar}
					FROM AppBundle\Entity\User u
					WHERE u.id != :threadAuthor 
					AND u.id IN (
						SELECT IDENTITY(r.author) FROM AppBundle\Entity\Reply r WHERE r.thread = :thread
					)
					";
					$threadUnit['followedCommentators'] = $em->createQuery($dql)
																->setParameter(':thread', $threadUnit['thread'])
																->setParameter(':threadAuthor', $threadUnit['thread']->getAuthor())
																->setMaxResults(6)
																->getResult()
																;
				}
			}
            
            // Boardest Thread'leri getir
            // $q = $em->getRepository('AppBundle\Entity\Thread')->getBoardestThreads($request->getLocale());
            // $boardestThreadUnits = $paginator->paginate(
                // $q,
                // $request->query->getInt('page', 1), 10
            // );
			
			// Son cevaplanan Thread'leri getir
            $q = $em->getRepository('AppBundle\Entity\Thread')->getLatestRepliedThreads($request->getLocale());
			$latestRepliedThreadUnits = $paginator->paginate(
                $q,
                $request->query->getInt('page', 1), 10
            );
			
            // Tüm Topic'leri getir
            $topics = $em->getRepository('AppBundle\Entity\Topic')->getFeaturedTopicsForPublic();
            
            $mobileDetector = $this->get('mobile_detect.mobile_detector');
            if($mobileDetector->isMobile()){
                return $this->render('default/mobile/index-loggedout.html.twig', array(
                    'threadUnits' => $threadUnits,
                    // 'boardestThreadUnits' => $boardestThreadUnits,
                    'latestRepliedThreadUnits' => $latestRepliedThreadUnits,
                    'topics' => $topics,
					'page' => array(
						'swipemenu' => 1
					)
                ));
            }else{
                return $this->render('default/index-loggedout.html.twig', array(
                    'threadUnits' => $threadUnits,
                    // 'boardestThreadUnits' => $boardestThreadUnits,
                    'latestRepliedThreadUnits' => $latestRepliedThreadUnits,
                    'topics' => $topics
                ));
            }
            
            
        // Eğer oturum açıksa
        }else{
			
			if($request->query->get('feed') == 'all'){
				$emptyFeed = 0;
				$q = $em->getRepository('AppBundle\Entity\Thread')
						->getThreads(array(
							'appuser' => $this->getUser(),
							'type' => $tab,
							'locale' => $request->getLocale(),
							'env' => $this->container->get('kernel')->getEnvironment(),
							'sorting' => $request->query->get('sorting'),
							'date' => $request->query->get('date')
						));
				$paginator  = $this->get('knp_paginator');
				$threadUnits = $paginator->paginate(
					$q,
					$request->query->getInt('page', 1), 10
				);
			}elseif($request->query->get('feed') == 'followed-users'){
				$emptyFeed = 0;
				$q = $em->getRepository('AppBundle\Entity\Thread')
						->getThreadsForUserByFollowedUsers(array(
							'appuser' => $this->getUser(),
							'type' => $tab,
							'locale' => $request->getLocale(),
							'env' => $this->container->get('kernel')->getEnvironment(),
							'sorting' => $request->query->get('sorting'),
							'date' => $request->query->get('date')
						));
				$paginator  = $this->get('knp_paginator');
				$threadUnits = $paginator->paginate(
					$q,
					$request->query->getInt('page', 1), 10
				);
			}else{
				// User'ın abone olduğu Thread'leri getir
				$q = $em->getRepository('AppBundle\Entity\Thread')
						->getThreadsForUser(array(
							'appuser' => $this->getUser(),
							'type' => $tab,
							'locale' => $request->getLocale(),
							'env' => $this->container->get('kernel')->getEnvironment(),
							'sorting' => $request->query->get('sorting'),
							'date' => $request->query->get('date')
						));

				$paginator  = $this->get('knp_paginator');
				$threadUnits = $paginator->paginate(
					$q,
					$request->query->getInt('page', 1), 10
				);
				
				// Eğer User ve Topic takip etmiyorsa tüm Thread'leri getir
				if($this->getUser()->getTopicsFollowed()->count() <= 1){
					$emptyFeed = 1;
					$q = $em->getRepository('AppBundle\Entity\Thread')
							->getThreads(array(
								'appuser' => $this->getUser(),
								'type' => $tab,
								'locale' => $request->getLocale(),
								'env' => $this->container->get('kernel')->getEnvironment(),
								'sorting' => $request->query->get('sorting'),
								'date' => $request->query->get('date')
							));

					$paginator  = $this->get('knp_paginator');
					$threadUnits = $paginator->paginate(
						$q,
						$request->query->getInt('page', 1), 10
					);
				}else{
					$emptyFeed = 0;
				}
			}
			
			// Yorum baloncuklarını getir
			if($threadUnits->items){
				foreach($threadUnits->items as &$threadUnit){
					$dql = "SELECT partial u.{id, name, username, avatar}
					FROM AppBundle\Entity\User u
					WHERE u.id != :appuser 
					AND u.id != :threadAuthor 
					AND u.id IN (
						SELECT IDENTITY(uf.user) FROM AppBundle\Entity\UserFollow uf WHERE uf.owner = :appuser
					)
					AND u.id IN (
						SELECT IDENTITY(r.author) FROM AppBundle\Entity\Reply r WHERE r.thread = :thread
					)
					";
					$threadUnit['followedCommentators'] = $em->createQuery($dql)
																->setParameter(':appuser', $this->getUser())
																->setParameter(':thread', $threadUnit['thread'])
																->setParameter(':threadAuthor', $threadUnit['thread']->getAuthor())
																->setMaxResults(6)
																->getResult()
																;
				}
			}
			
            // Takip ettiği Thread'leri getir
            $followedThreadUnits = $em->getRepository('AppBundle\Entity\Thread')->getFollowedThreads($this->getUser(), $request->getLocale());
            
            // Boardest Thread'leri getir
            // $q = $em->getRepository('AppBundle\Entity\Thread')->getBoardestThreads($request->getLocale());
            // $boardestThreadUnits = $paginator->paginate(
                // $q,
                // $request->query->getInt('page', 1), 10
            // );
			
			// Son cevaplanan Thread'leri getir
            $q = $em->getRepository('AppBundle\Entity\Thread')->getLatestRepliedThreads($request->getLocale());
			$latestRepliedThreadUnits = $paginator->paginate(
                $q,
                $request->query->getInt('page', 1), 10
            );
            
            // Okunmamış Notification sayısını getir
            $this->getUser()->meta['notifications']['has'] = $em->getRepository('AppBundle\Entity\Notification')
                                                                ->getNotificationCountByUser($this->getUser());
																
            // Okunmamış PrivateMessage sayısını getir
            $this->getUser()->meta['messages']['has'] = $em->getRepository('AppBundle\Entity\PrivateMessage')
                                                                ->getUnseenMessageCountForUser($this->getUser());
																
            // Tüm Topic'leri getir
            $topics = $em->getRepository('AppBundle\Entity\Topic')->getFeaturedTopicsForPublic();
            
            // Featured Rule'ları getir
			$dql = "SELECT r.id, r.title, r.ruleType FROM AppBundle\Entity\Rule r WHERE r.isFeatured = 1 ORDER BY r.ruleType DESC, r.title ASC";
			$rules = $em->createQuery($dql)
						->setHint(
                                    \Doctrine\ORM\Query::HINT_CUSTOM_OUTPUT_WALKER,
                                    'Gedmo\\Translatable\\Query\\TreeWalker\\TranslationWalker'
                                )
						->getResult();
						
            $mobileDetector = $this->get('mobile_detect.mobile_detector');
            if($mobileDetector->isMobile()){
                return $this->render('default/mobile/index-loggedin.html.twig', array(
                    'topics' => $topics,
                    'threadUnits' => $threadUnits,
                    'followedThreadUnits' => $followedThreadUnits,
                    // 'boardestThreadUnits' => $boardestThreadUnits,
                    'latestRepliedThreadUnits' => $latestRepliedThreadUnits,
					'emptyFeed' => $emptyFeed,
					'page' => array(
						'swipemenu' => 1
					)
                ));
            }else{
                return $this->render('default/index-loggedin.html.twig', array(
                    'topics' => $topics,
                    'threadUnits' => $threadUnits,
                    'followedThreadUnits' => $followedThreadUnits,
                    // 'boardestThreadUnits' => $boardestThreadUnits,
                    'latestRepliedThreadUnits' => $latestRepliedThreadUnits,
					'emptyFeed' => $emptyFeed,
					'rules' => $rules
                ));
            }
            
        }
    } //
    
    /**
     * @Route("/ajax/feed/home/{tab}", name="homepage_feed", host="{_locale}.%base_host%", condition="request.isXmlHttpRequest()", defaults={"tab": "all"}, requirements={
     *     "tab": "all|questions|links|threads",
     *     "_locale": "%app.locales%"
     * })
     */
    public function feedAction(Request $request, $tab)
    {
        $em = $this->getDoctrine()->getManager();
        
        // Eğer oturum açık değilse
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            // Thread'leri getir            
            $q = $em->getRepository('AppBundle\Entity\Thread')
                    ->getThreads(array(
						'appuser' => $this->getUser(),
                        'type' => $tab,
                        'locale' => $request->getLocale(),
						'env' => $this->container->get('kernel')->getEnvironment(),
						'sorting' => $request->query->get('sorting'),
						'date' => $request->query->get('date')
                    ));

            $paginator  = $this->get('knp_paginator');
            $threadUnits = $paginator->paginate(
                $q,
                $request->query->getInt('page', 1), 10
            );
            
            $renderAll = '';
            foreach($threadUnits->items as $threadUnit){
				$dql = "SELECT partial u.{id, name, username, avatar}
					FROM AppBundle\Entity\User u
					WHERE u.id != :threadAuthor 
					AND u.id IN (
						SELECT IDENTITY(r.author) FROM AppBundle\Entity\Reply r WHERE r.thread = :thread
					)
					";
					$threadUnit['followedCommentators'] = $em->createQuery($dql)
																->setParameter(':thread', $threadUnit['thread'])
																->setParameter(':threadAuthor', $threadUnit['thread']->getAuthor())
																->setMaxResults(6)
																->getResult()
																;
				
                $renderAll .= $this->renderView('default/snippets/thread-single-in-list.html.twig', array(
                    'threadUnit' => $threadUnit
                ));
            }
            
            return new Response($renderAll);
            
        // Eğer oturum açıksa
        }else{            
            $q = $em->getRepository('AppBundle\Entity\Thread')
                    ->getThreadsForUser(array(
                        'appuser' => $this->getUser(),
                        'type' => $tab,
                        'locale' => $request->getLocale(),
						'env' => $this->container->get('kernel')->getEnvironment(),
						'sorting' => $request->query->get('sorting'),
						'date' => $request->query->get('date')
                    ));

            $paginator  = $this->get('knp_paginator');
            $threadUnits = $paginator->paginate(
                $q,
                $request->query->getInt('page', 1), 10
            );
			
			if($request->query->get('feed') == 'all'){
				$emptyFeed = 0;
				$q = $em->getRepository('AppBundle\Entity\Thread')
						->getThreads(array(
							'appuser' => $this->getUser(),
							'type' => $tab,
							'locale' => $request->getLocale(),
							'env' => $this->container->get('kernel')->getEnvironment(),
							'sorting' => $request->query->get('sorting'),
							'date' => $request->query->get('date')
						));
				$paginator  = $this->get('knp_paginator');
				$threadUnits = $paginator->paginate(
					$q,
					$request->query->getInt('page', 1), 10
				);
			}elseif($request->query->get('feed') == 'followed-users'){
				$emptyFeed = 0;
				$q = $em->getRepository('AppBundle\Entity\Thread')
						->getThreadsForUserByFollowedUsers(array(
							'appuser' => $this->getUser(),
							'type' => $tab,
							'locale' => $request->getLocale(),
							'env' => $this->container->get('kernel')->getEnvironment(),
							'sorting' => $request->query->get('sorting'),
							'date' => $request->query->get('date')
						));
				$paginator  = $this->get('knp_paginator');
				$threadUnits = $paginator->paginate(
					$q,
					$request->query->getInt('page', 1), 10
				);
			}else{
				// Eğer User ve Topic takip etmiyorsa tüm Thread'leri getir
				if($this->getUser()->getTopicsFollowed()->count() <= 1){
					$emptyFeed = 1;
					$q = $em->getRepository('AppBundle\Entity\Thread')
							->getThreads(array(
								'appuser' => $this->getUser(),
								'type' => $tab,
								'locale' => $request->getLocale()
							));

					$paginator  = $this->get('knp_paginator');
					$threadUnits = $paginator->paginate(
						$q,
						$request->query->getInt('page', 1), 10
					);
				}else{
					$emptyFeed = 0;
				}
			}
            
            $renderAll = '';
            foreach($threadUnits->items as $threadUnit){
				$dql = "SELECT partial u.{id, name, username, avatar}
				FROM AppBundle\Entity\User u
				WHERE u.id != :appuser 
				AND u.id != :threadAuthor 
				AND u.id IN (
					SELECT IDENTITY(uf.user) FROM AppBundle\Entity\UserFollow uf WHERE uf.owner = :appuser
				)
				AND u.id IN (
					SELECT IDENTITY(r.author) FROM AppBundle\Entity\Reply r WHERE r.thread = :thread
				)
				";
				$threadUnit['followedCommentators'] = $em->createQuery($dql)
															->setParameter(':appuser', $this->getUser())
															->setParameter(':thread', $threadUnit['thread'])
															->setParameter(':threadAuthor', $threadUnit['thread']->getAuthor())
															->setMaxResults(6)
															->getResult()
															;
															
                $renderAll .= $this->renderView('default/snippets/thread-single-in-list.html.twig', array(
                    'threadUnit' => $threadUnit,
					'emptyFeed' => $emptyFeed
                ));
            }
            
            return new Response($renderAll);
        }
    } //
}

/**
 * Messenger
 */
 
 
/* Toggle Message */
$(document).on('click', '.messenger .message-unit .header', function(){
	var el = $(this);
	var unit = el.closest('.message-unit');
	
	unit.toggleClass('folded open');
});

/* Send Message */
$(document).on('submit', '.jsSendMessage', function(e){
    e.preventDefault();
	
	var el = $(this);
	var composer = el.find('.redactor-editor');
	var url = el.attr('action');
	var data = el.serialize();
	var message_list = $('.message-list');
	var conversation_list = $('.conversation-list');
	
	$.ajax({
		url: url,
		method: 'post',
		data: data,
		beforeSend: function(){
			mainLoaderShow();
		}
	}).done(function(resp){
		if(resp.status){
			message_list.prepend(resp.render.message);
			
			if($('#conversation_' + resp.targets.conversation_id).length){
				$('#conversation_' + resp.targets.conversation_id).replaceWith(resp.render.conversation);
			}else{
				conversation_list.prepend(resp.render.conversation);				
			}
			
			// Reorder Conversations
			var orderedConversations = $('.conversation-list .unit').sort(function(a, b){
				return $(b).attr('data-date') - $(a).attr('data-date');
			});
			
			$('.conversation-list').html(orderedConversations);
			$('.composer-ta').val('');
			
			composer.html('').addClass('redactor-placeholder');
			
			$('.jsDeleteConversationBtn').removeClass('hide').prependTo($('.jsDeleteConversationBtn').parent());
			
			if(resp.redirect){
				document.location = resp.redirect;
			}
		}else{
			pushNotif(Translator.trans('message.gone_wrong', {}, 'stack'));
		}
	}).always(function(){
		mainLoaderHide();
	});	
});

/* Delete Message */
$(document).on('submit', '.jsDeleteMessageViaForm', function (e) {
    e.preventDefault();

    var el = $(this);
    var url = el.attr('action');
    var data = el.serialize();
    
    $.ajax({
        url: url,
        method: 'post',
        data: data,
        cache: false,
        beforeSend: function () {
            mainLoaderShow();
        }
    }).done(function(resp){
        
        if(resp.status){
            $('#message_' + resp.targets.message_id).remove();
			
			if(!$('.message-list .message-unit').length){
				$('.message-list').empty();
				$('.jsDeleteConversationBtn').addClass('hide').appendTo($('.jsDeleteConversationBtn').parent());
				
				$('#conversation_' + resp.targets.conversation_id).remove();
				
				if(!$('.conversation-list .unit').length){
					$('.conversation-list').empty();
				}
			}
							
			// If conversation render is found
			if(resp.render.conversation){
				if($('#conversation_' + resp.targets.conversation_id).length){
					$('#conversation_' + resp.targets.conversation_id).replaceWith(resp.render.conversation);
				}else{
					conversation_list.prepend(resp.render.conversation);				
				}
			}
        }else{
			pushNotif(Translator.trans('message.gone_wrong', {}, 'stack'));
		}
        
    }).fail(function(){
        mainLoaderHide();
    }).always(function(){
        $.magnificPopup.close();
        mainLoaderHide();
    });

});

/* Delete Conversation */
$(document).on('submit', '.jsDeleteConversationViaForm', function (e) {
    e.preventDefault();

    var el = $(this);
    var url = el.attr('action');
    var data = el.serialize();
    
    $.ajax({
        url: url,
        method: 'post',
        data: data,
        cache: false,
        beforeSend: function () {
            mainLoaderShow();
        }
    }).done(function(resp){
        
        if(resp.status){
            $('.message-list').empty();
            $('#conversation_' + resp.target_id).remove();
				$('.jsDeleteConversationBtn').addClass('hide').appendTo($('.jsDeleteConversationBtn').parent());
			
			if(!$('.conversation-list .unit').length){
				$('.conversation-list').empty();
			}
        }
        
    }).fail(function(){
        mainLoaderHide();
    }).always(function(){
        $.magnificPopup.close();
        mainLoaderHide();
    });

});

/* Listen */
function listenMessenger(){
	var listen_url = $('.messenger').attr('data-listen-url');
	
	var source = new EventSource(listen_url);
	
	source.onmessage = function(e) {
		resp = JSON.parse(e.data);
		var currentHtml = $('.left-col').html();
		$('.conversation-list').html(resp.renders.conversations);
		
		if(resp.newMessageCount){
			$('.message-list').prepend(resp.renders.messages);
		}
	};
}

listenMessenger();

$('.conversations .scrollbar-macosx').scrollbar();

/* Pager */
var conversationPagerOffset = 2;
$('.conversation-body .scrollbar-macosx').scrollbar({
    "onScroll": function(y, x){
        if(y.scroll == y.maxScroll){
			
            var el = $('.conversation-body');
			var url = el.attr('data-fetch');
			var target = $('.message-list');
			var appendedMessageCount = $('.message-unit.appended').length;
			var notAppendedMessageCount = $('.message-unit:not(.appended)').length;
			
			if(el.attr('data-listen') == 'false'){
				return false;
			}
			
			$.ajax({
				url: url,
				method: 'get',
				dataType: 'json',
				data: {'page': conversationPagerOffset, 'offset': notAppendedMessageCount + appendedMessageCount},
				beforeSend: function(){
					mainLoaderShow();
				}
			}).done(function (resp) {
				if (resp.status) {
					conversationPagerOffset++;
					target.append(resp.render);
				} else {
					el.attr('data-listen', 'false');
				}
			}).always(function(){
				mainLoaderHide();
			});
        }
    }
});